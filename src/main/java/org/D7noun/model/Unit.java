package org.D7noun.model;

import javax.persistence.Entity;
import java.io.Serializable;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Column;
import javax.persistence.Version;
import java.util.Set;
import java.util.HashSet;
import org.D7noun.model.Person;
import javax.persistence.OneToMany;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity(name="unit")
public class Unit implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "id", updatable = false, nullable = false)
	private Long id;
	@Version
	@Column(name = "version")
	private int version;

	
	@Column(unique=true,nullable = false)
	private String uid;

	@Column(unique=true)
	private String name;

	@Column
	private String facilityLocation;

	@Column
	private String departments;

	public String getDepartments() {
		return departments;
	}

	public void setDepartments(String departments) {
		this.departments = departments;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@OneToMany(targetEntity=Person.class,mappedBy = "unit", cascade = CascadeType.ALL)
	private Set<Person> persons = new HashSet<Person>();

	public Long getId() {
		return this.id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public int getVersion() {
		return this.version;
	}

	public void setVersion(final int version) {
		this.version = version;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Unit)) {
			return false;
		}
		Unit other = (Unit) obj;
		if (id != null) {
			if (!id.equals(other.id)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		String result = getClass().getSimpleName() + " ";
		if (uid != null && !uid.trim().isEmpty())
			result += "uid: " + uid;
		if (name != null && !name.trim().isEmpty())
			result += ", name: " + name;
		return result;
	}

	public Set<Person> getPersons() {
		return this.persons;
	}

	public void setPersons(final Set<Person> persons) {
		this.persons = persons;
	}

	/**
	 * @return the facilityLocation
	 */
	public String getFacilityLocation() {
		return facilityLocation;
	}

	/**
	 * @param facilityLocation
	 *            the facilityLocation to set
	 */
	public void setFacilityLocation(String facilityLocation) {
		this.facilityLocation = facilityLocation;
	}
}