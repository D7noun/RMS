package org.D7noun.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

@NamedQueries({ @NamedQuery(name = Item.allAvailableAccessories, query = "SELECT i FROM Item i WHERE i.state = ?1 AND i.id <> ?2"),
		@NamedQuery(name = Item.findItemBySerialNumber, query = "SELECT i FROM Item i where i.serialNumber = ?1"),

})
@Entity(name="Item")
public class Item implements Serializable {

	public static final String allAvailableAccessories = "allAvailableAccessories";
	public static final String findItemBySerialNumber = "findItemBySerialNumber";
	public static final String findAccessoriesByItemSerialNumber = "findAccessoriesByItemSerialNumber";
	public static final String findAccessoriesByItemId = "findAccessoriesByItemId";

	/**
	 * 
	 * */
	public enum State {
		under_maintenance, damaged, rented, available, not_available;
	}

	/**
	 * 
	 * */

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "id", updatable = false, nullable = false)
	private Long id;
	@Version
	@Column(name = "version")
	private int version;

	@Column(nullable = false,unique=true)
	private String name;

	@Column
	private String serialNumber;

	@Column(nullable = false,unique=true)
	private String deviceId;

	@Column
	private String model;

	@Column
	private String origin;

	@ManyToOne(targetEntity=Company.class)
	private Company manufacturingComapny;

	@ManyToOne(targetEntity=Company.class)
	private Company purchaseCompany;

	@Column
	@Temporal(TemporalType.DATE)
	private Date manufactureDate;

	@Column
	@Temporal(TemporalType.DATE)
	private Date purchaseDate;

	@Column
	private double purchasePrice;

	@Column
	private double actualPrice;

	@Column
	private String specilities;

	@Column
	private String usages;

	@Column
	private String domains;

	@Column
	private String types;

	@Column
	private boolean firstHand = false;

	@Column
	private boolean evacuation = false;

	@Column
	@Enumerated(EnumType.STRING)
	private State state=State.available;

	@ManyToOne(targetEntity=Person.class)
	private Person personInCharge;

	@OneToMany(targetEntity=AttachmentItem.class,cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
	private List<AttachmentItem> attachments = new ArrayList<AttachmentItem>();

	@JoinColumn
	@OneToMany(targetEntity=Item.class,cascade = CascadeType.MERGE, orphanRemoval = true, fetch = FetchType.EAGER)
	private List<Item> accessories = new ArrayList<Item>();



	/**
	 * 
	 * D7noun: GETTERS&SETTERS
	 * 
	 */

	public String getSpecilities() {
		return specilities;
	}

	public void setSpecilities(String specilities) {
		this.specilities = specilities;
	}

	public String getUsages() {
		return usages;
	}

	public void setUsages(String usages) {
		this.usages = usages;
	}

	public String getDomains() {
		return domains;
	}

	public void setDomains(String domains) {
		this.domains = domains;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public int getVersion() {
		return this.version;
	}

	public void setVersion(final int version) {
		this.version = version;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public Date getManufactureDate() {
		return manufactureDate;
	}

	public void setManufactureDate(Date manufactureDate) {
		this.manufactureDate = manufactureDate;
	}

	public Date getPurchaseDate() {
		return purchaseDate;
	}

	public void setPurchaseDate(Date purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public boolean getFirstHand() {
		return firstHand;
	}

	public void setFirstHand(boolean firstHand) {
		this.firstHand = firstHand;
	}

	public List<Item> getAccessories() {
		return accessories;
	}

	public void setAccessories(List<Item> accessories) {
		this.accessories = accessories;
	}

	public void addAccessory(Item accessory) {
		this.getAccessories().add(accessory);

	}

	/**
	 * @return the deviceId
	 */
	public String getDeviceId() {
		return deviceId;
	}

	/**
	 * @param deviceId
	 *            the deviceId to set
	 */
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	/**
	 * @return the purchasePrice
	 */
	public double getPurchasePrice() {
		return purchasePrice;
	}

	/**
	 * @param purchasePrice
	 *            the purchasePrice to set
	 */
	public void setPurchasePrice(double purchasePrice) {
		this.purchasePrice = purchasePrice;
	}

	/**
	 * @return the actualPrice
	 */
	public double getActualPrice() {
		return actualPrice;
	}

	/**
	 * @param actualPrice
	 *            the actualPrice to set
	 */
	public void setActualPrice(double actualPrice) {
		this.actualPrice = actualPrice;
	}

	/**
	 * @return the types
	 */
	public String getTypes() {
		return types;
	}

	/**
	 * @param types
	 *            the types to set
	 */
	public void setTypes(String types) {
		this.types = types;
	}

	/**
	 * @return the evacuation
	 */
	public boolean isEvacuation() {
		return evacuation;
	}

	/**
	 * @param evacuation
	 *            the evacuation to set
	 */
	public void setEvacuation(boolean evacuation) {
		this.evacuation = evacuation;
	}

	/**
	 * @return the personInCharge
	 */
	public Person getPersonInCharge() {
		return personInCharge;
	}

	/**
	 * @param personInCharge
	 *            the personInCharge to set
	 */
	public void setPersonInCharge(Person personInCharge) {
		this.personInCharge = personInCharge;
	}

	/**
	 * @return the finditembyserialnumber
	 */
	public static String getFinditembyserialnumber() {
		return findItemBySerialNumber;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the attachments
	 */
	

	public String displayName() {
		String displayName = "";
		if (deviceId != null && !deviceId.isEmpty()) {
			displayName += deviceId;
			displayName += " | ";
		}
		if (name != null && !name.isEmpty()) {
			displayName += name;
			displayName += " | ";
		}
		if (model != null && !model.isEmpty()) {
			displayName += model;
			displayName += " | ";
		}
		if (serialNumber != null && !serialNumber.isEmpty()) {
			displayName += serialNumber;
			displayName += " | ";
		}
		return displayName;
	}

	public List<AttachmentItem> getAttachments() {
		return attachments;
	}

	public void setAttachments(List<AttachmentItem> attachments) {
		this.attachments = attachments;
	}

	

	/**
	 * @return the state
	 */
	public State getState() {
		return state;
	}

	/**
	 * @param state
	 *            the state to set
	 */
	public void setState(State state) {
		this.state = state;
	}

	public boolean canBeChanged() {
		if (state == State.available || state == State.not_available) {
			return false;
		}
		return true;
	}

	public String getAccessoriesAsString() {
		String text = "";
		if (accessories != null && !accessories.isEmpty()) {
			for (Item accessory : accessories) {
				text += accessory.getId();
			}
		}
		return text;
	}

	public boolean hasPicture() {
		// TODO
		return true;
	}

	/**
	 * @return the manufacturingComapny
	 */
	public Company getManufacturingComapny() {
		return manufacturingComapny;
	}

	/**
	 * @param manufacturingComapny
	 *            the manufacturingComapny to set
	 */
	public void setManufacturingComapny(Company manufacturingComapny) {
		this.manufacturingComapny = manufacturingComapny;
	}

	/**
	 * @return the purchaseCompany
	 */
	public Company getPurchaseCompany() {
		return purchaseCompany;
	}

	/**
	 * @param purchaseCompany
	 *            the purchaseCompany to set
	 */
	public void setPurchaseCompany(Company purchaseCompany) {
		this.purchaseCompany = purchaseCompany;
	}

}