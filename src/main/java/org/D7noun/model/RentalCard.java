package org.D7noun.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

@NamedQueries({
		@NamedQuery(name = RentalCard.getRentalCardsByItem, query = "SELECT r FROM RentalCard r WHERE r.item = :item"),
		@NamedQuery(name = RentalCard.deleteRentalCardsByItem, query = "DELETE FROM RentalCard r WHERE r.item = :item") })
@Entity(name="RentalCard")
public class RentalCard implements Serializable {

	public static final String getRentalCardsByItem = "getRentalCardsByItem";
	public static final String deleteRentalCardsByItem = "deleteRentalCardsByItem";

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "id", updatable = false, nullable = false)
	private Long id;
	@Version
	@Column(name = "version")
	private int version;

	@Column
	@Temporal(TemporalType.DATE)
	private Date startDate;

	@Column
	@Temporal(TemporalType.DATE)
	private Date endDate;

	@Column
	@Temporal(TemporalType.DATE)
	private Date dueDate;

	@OneToOne(targetEntity=Person.class,cascade = CascadeType.MERGE)
	private Person user;


	@Column(length = 1000)
	private String note;

	@OneToOne(targetEntity=Item.class,cascade = CascadeType.MERGE)
	private Item item;

	@OneToMany(targetEntity=AttachmentRentalCard.class,mappedBy = "rentalCard", cascade = CascadeType.MERGE, orphanRemoval = true, fetch = FetchType.EAGER)
	private Set<AttachmentRentalCard> attachments = new HashSet<AttachmentRentalCard>();

	public Long getId() {
		return this.id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public int getVersion() {
		return this.version;
	}

	public void setVersion(final int version) {
		this.version = version;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof RentalCard)) {
			return false;
		}
		RentalCard other = (RentalCard) obj;
		if (id != null) {
			if (!id.equals(other.id)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public Person getUser() {
		return user;
	}

	public void setUser(Person user) {
		this.user = user;
	}


	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public void newUser() {
		this.user = new Person();
	}

	
	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	@Override
	public String toString() {
		String result = getClass().getSimpleName() + " ";
		if (note != null && !note.trim().isEmpty())
			result += "note: " + note;
		return result;
	}

	/**
	 * @return the attachments
	 */
	public Set<AttachmentRentalCard> getAttachments() {
		return attachments;
	}

	/**
	 * @param attachments
	 *            the attachments to set
	 */
	public void setAttachments(Set<AttachmentRentalCard> attachments) {
		this.attachments = attachments;
	}

}