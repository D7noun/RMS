package org.D7noun.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.TemporalType;
import javax.persistence.Temporal;
import javax.persistence.Version;

@NamedQueries({
		@NamedQuery(name = Damaged.getDamagedCardsByItemId, query = "SELECT d FROM Damaged d WHERE d.item = :item"),
		@NamedQuery(name = Damaged.deleteDamagedCardsByItem, query = "DELETE FROM Damaged d WHERE d.item = :item") })
@Entity(name="Damaged")
public class Damaged implements Serializable {

	public static final String getDamagedCardsByItemId = "getDamagedCardsByItemId";
	public static final String deleteDamagedCardsByItem = "deleteDamagedCardsByItem";

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "id", updatable = false, nullable = false)
	private Long id;

	@Version
	@Column(name = "version")
	private int version;

	@Temporal(TemporalType.DATE)
	private Date date;

	@Column
	private String reason;

	@OneToOne(targetEntity=Item.class,cascade = CascadeType.MERGE)
	private Item item;

	@OneToOne(targetEntity=Person.class,cascade = CascadeType.MERGE)
	private Person personInCharge;

	@OneToMany(targetEntity=AttachmentDamaged.class,mappedBy = "damaged", cascade = CascadeType.MERGE, orphanRemoval = true, fetch = FetchType.EAGER)
	private Set<AttachmentDamaged> attachments = new HashSet<AttachmentDamaged>();

	@Column(length = 1000)
	private String note;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the version
	 */
	public int getVersion() {
		return version;
	}

	/**
	 * @param version
	 *            the version to set
	 */
	public void setVersion(int version) {
		this.version = version;
	}

	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * @param date
	 *            the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * @return the reason
	 */
	public String getReason() {
		return reason;
	}

	/**
	 * @param reason
	 *            the reason to set
	 */
	public void setReason(String reason) {
		this.reason = reason;
	}

	/**
	 * @return the item
	 */
	public Item getItem() {
		return item;
	}

	/**
	 * @param item
	 *            the item to set
	 */
	public void setItem(Item item) {
		this.item = item;
	}

	/**
	 * @return the personInCharge
	 */
	public Person getPersonInCharge() {
		return personInCharge;
	}

	/**
	 * @param personInCharge
	 *            the personInCharge to set
	 */
	public void setPersonInCharge(Person personInCharge) {
		this.personInCharge = personInCharge;
	}

	/**
	 * @return the attachments
	 */
	public Set<AttachmentDamaged> getAttachments() {
		return attachments;
	}

	/**
	 * @param attachments
	 *            the attachments to set
	 */
	public void setAttachments(Set<AttachmentDamaged> attachments) {
		this.attachments = attachments;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void newItem() {
		this.item = new Item();
	}

	public void newPersonInCharge() {
		this.personInCharge = new Person();
	}

}
