package org.D7noun.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Version;

@Entity
public class Person implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "id", updatable = false, nullable = false)
	private Long id;
	@Version
	@Column(name = "version")
	private int version;

	@Column(nullable = false)
	private String name;

	@Column
	private String department;

	@Column
	private String facilityLocation;
	
	
	@ManyToOne(targetEntity=Unit.class,cascade = CascadeType.MERGE)
	private Unit unit;

	@Column(nullable = false,unique=true)
	private String pid;


	@OneToOne(targetEntity=RentalCard.class,mappedBy = "user")
	private RentalCard rentalCard;

	public Long getId() {
		return this.id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public int getVersion() {
		return this.version;
	}

	public void setVersion(final int version) {
		this.version = version;
	}

	public Person() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Person)) {
			return false;
		}
		Person other = (Person) obj;
		if (id != null) {
			if (!id.equals(other.id)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getFacilityLocation() {
		return facilityLocation;
	}

	public void setFacilityLocation(String facilityLocation) {
		this.facilityLocation = facilityLocation;
	}

	public Unit getUnit() {
		return this.unit;
	}

	public void setUnit(final Unit unit) {
		this.unit = unit;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	
	public RentalCard getRentalCard() {
		return rentalCard;
	}

	public void setRentalCard(RentalCard rentalCard) {
		this.rentalCard = rentalCard;
	}

	@Override
	public String toString() {
		String result = getClass().getSimpleName() + " ";
		if (name != null && !name.trim().isEmpty())
			result += "name: " + name;
		if (department != null && !department.trim().isEmpty())
			result += ", department: " + department;
		if (facilityLocation != null && !facilityLocation.trim().isEmpty())
			result += ", facilityLocation: " + facilityLocation;
		if (pid != null && !pid.trim().isEmpty())
			result += ", pid: " + pid;
		return result;
	}


	public void newRentalCard() {
		this.rentalCard = new RentalCard();
	}
}