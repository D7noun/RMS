package org.D7noun.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

@NamedQueries({
		@NamedQuery(name = Maintainence.getMaintenanceCardsByItem, query = "SELECT m FROM Maintainence m WHERE m.item = :item"),
		@NamedQuery(name = Maintainence.deleteMaintenanceCardsByItem, query = "DELETE FROM Maintainence m WHERE m.item = :item") })
@Entity(name="Maintainence")
public class Maintainence implements Serializable {

	public static final String getMaintenanceCardsByItem = "getMaintenanceCardsByItem";
	public static final String deleteMaintenanceCardsByItem = "deleteMaintenanceCardsByItem";

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "id", updatable = false, nullable = false)
	private Long id;
	@Version
	@Column(name = "version")
	private int version;

	@OneToOne(targetEntity=Item.class,cascade = CascadeType.MERGE)
	private Item item;

	@Column
	@Temporal(TemporalType.DATE)
	private Date startDate;

	@Column
	@Temporal(TemporalType.DATE)
	private Date endDate;

	@Column
	@Temporal(TemporalType.DATE)
	private Date dueDate;

	@Column
	private String malfunctioning;

	@Column
	private Double fees;

	@ManyToOne(targetEntity=Company.class)
	private Company maintenanceCompany;

	@Column
	private String maintenanceMan;

	@OneToMany(targetEntity=AttachmentMaintenance.class,mappedBy = "maintainence", cascade = CascadeType.MERGE, orphanRemoval = true, fetch = FetchType.EAGER)
	private Set<AttachmentMaintenance> attachments = new HashSet<AttachmentMaintenance>();

	public Long getId() {
		return this.id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public int getVersion() {
		return this.version;
	}

	public void setVersion(final int version) {
		this.version = version;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Maintainence)) {
			return false;
		}
		Maintainence other = (Maintainence) obj;
		if (id != null) {
			if (!id.equals(other.id)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public String getMalfunctioning() {
		return malfunctioning;
	}

	public void setMalfunctioning(String malfunctioning) {
		this.malfunctioning = malfunctioning;
	}

	public Double getFees() {
		return fees;
	}

	public void setFees(Double fees) {
		this.fees = fees;
	}

	public String getMaintenanceMan() {
		return maintenanceMan;
	}

	public void setMaintenanceMan(String maintenanceMan) {
		this.maintenanceMan = maintenanceMan;
	}

	@Override
	public String toString() {
		String result = getClass().getSimpleName() + " ";
		if (malfunctioning != null && !malfunctioning.trim().isEmpty())
			result += "malfunctioning: " + malfunctioning;
		if (fees != null)
			result += ", fees: " + fees;
		if (maintenanceMan != null && !maintenanceMan.trim().isEmpty())
			result += ", maintenanceMan: " + maintenanceMan;
		return result;
	}

	public void newItem() {
		this.item = new Item();
	}

	/**
	 * @return the attachments
	 */
	public Set<AttachmentMaintenance> getAttachments() {
		return attachments;
	}

	/**
	 * @param attachments
	 *            the attachments to set
	 */
	public void setAttachments(Set<AttachmentMaintenance> attachments) {
		this.attachments = attachments;
	}

	/**
	 * @return the maintenanceCompany
	 */
	public Company getMaintenanceCompany() {
		return maintenanceCompany;
	}

	/**
	 * @param maintenanceCompany
	 *            the maintenanceCompany to set
	 */
	public void setMaintenanceCompany(Company maintenanceCompany) {
		this.maintenanceCompany = maintenanceCompany;
	}

}