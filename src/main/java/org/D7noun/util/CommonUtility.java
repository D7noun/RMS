package org.D7noun.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CommonUtility {

	public static List<String> getListFromString(String string) {
		List<String> list = new ArrayList<String>();
		if (string != null && !string.equals("")) {
			list = Arrays.asList(string.split("\\s*,\\s*"));
			return list;
		} else {
			return null;
		}
	}

}
