package org.D7noun.converter;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.D7noun.facade.PersonFacade;
import org.D7noun.model.Person;


@ManagedBean
public class PersonConverter implements Converter {

	/**
	 * 
	 */
	@EJB
	private PersonFacade personBean;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		try {
			if (value != null && !value.isEmpty()) {
				return personBean.find(Long.parseLong(value));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value != null) {
			Person person = (Person) value;
			return String.valueOf(person.getId());
		}
		return null;
	}

	/**
	 * @return the personBean
	 */
	public PersonFacade getPersonBean() {
		return personBean;
	}

	/**
	 * @param personBean
	 *            the personBean to set
	 */
	public void setPersonBean(PersonFacade personBean) {
		this.personBean = personBean;
	}

}
