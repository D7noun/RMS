package org.D7noun.converter;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.D7noun.facade.ItemFacade;
import org.D7noun.model.Item;


@ManagedBean
public class ItemConverter implements Converter {

	/**
	 * 
	 */
	@EJB
	private ItemFacade itemBean;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		try {
			if (value != null && !value.isEmpty()) {
				return itemBean.find(Long.parseLong(value));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value != null) {
			Item item = (Item) value;
			return String.valueOf(item.getId());
		}
		return null;
	}

}
