package org.D7noun.converter;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.D7noun.facade.CompanyFacade;
import org.D7noun.model.Company;


@ManagedBean
public class CompanyConverter implements Converter {

	/**
	 * 
	 */
	@EJB
	private CompanyFacade companyConverter;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		try {
			if (value != null && !value.isEmpty()) {
				return companyConverter.find(Long.parseLong(value));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value != null) {
			Company comapny = (Company) value;
			return String.valueOf(comapny.getId());
		}
		return null;
	}

}
