package org.D7noun.converter;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.D7noun.facade.UnitFacade;
import org.D7noun.model.Unit;


@ManagedBean
public class UnitConverter implements Converter {

	/**
	 * 
	 */
	@EJB
	private UnitFacade unitBean;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		try {
			if (value != null && !value.isEmpty()) {
				return unitBean.find(Long.parseLong(value));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value != null) {
			Unit unit = (Unit) value;
			return String.valueOf(unit.getId());
		}
		return null;
	}

	/**
	 * @return the unitBean
	 */
	public UnitFacade getUnitBean() {
		return unitBean;
	}

	/**
	 * @param unitBean
	 *            the unitBean to set
	 */
	public void setUnitBean(UnitFacade unitBean) {
		this.unitBean = unitBean;
	}

}
