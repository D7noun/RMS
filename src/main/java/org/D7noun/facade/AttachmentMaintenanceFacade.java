package org.D7noun.facade;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;

import org.D7noun.model.AttachmentMaintenance;
import org.D7noun.model.Maintainence;

@Stateful
@Local
public class AttachmentMaintenanceFacade implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@PersistenceContext(unitName = "ems-pu", type = PersistenceContextType.EXTENDED)
	private EntityManager entityManager;

	public AttachmentMaintenance save(AttachmentMaintenance attachment) {
		return entityManager.merge(attachment);
	}

	@SuppressWarnings("unchecked")
	public List<AttachmentMaintenance> findALl() {
		try {
			Query query = getEntityManager().createNamedQuery(AttachmentMaintenance.getAttacmentsByFileId,
					AttachmentMaintenance.class);
			return query.getResultList();
		} catch (Exception e) {
			System.err.println("D7noun: getAllValues");
			e.printStackTrace();
		}
		return null;
	}

	public void removeAttachmentFromTable(Maintainence maintainence, AttachmentMaintenance deletedAttachment) {
		try {
			deletedAttachment.setMaintainence(null);
			this.entityManager.merge(maintainence);
			this.entityManager.remove(this.entityManager.contains(deletedAttachment) ? deletedAttachment
					: this.entityManager.merge(deletedAttachment));
			this.entityManager.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * D7noun
	 * 
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
