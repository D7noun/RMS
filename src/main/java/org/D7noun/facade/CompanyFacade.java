package org.D7noun.facade;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.D7noun.model.Company;

@Stateless
public class CompanyFacade extends AbstarctFacade<Company> {

	public CompanyFacade() {
		super(Company.class);
		// TODO Auto-generated constructor stub
	}

	public CompanyFacade(Class<Company> clazz) {
		super(clazz);
		// TODO Auto-generated constructor stub
	}
	public List<Company> getAll() {

		CriteriaQuery<Company> criteria = getEm().getCriteriaBuilder().createQuery(Company.class);
		return getEm().createQuery(criteria.select(criteria.from(Company.class))).getResultList();
	}

	public long getCount(Company example) {

		CriteriaBuilder builder = getEm().getCriteriaBuilder();

		// Populate this.count

		CriteriaQuery<Long> countCriteria = builder.createQuery(Long.class);
		Root<Company> root = countCriteria.from(Company.class);
		countCriteria = countCriteria.select(builder.count(root)).where(getSearchPredicates(root,example));
		return getEm().createQuery(countCriteria).getSingleResult();

	}

	public List<Company> getPaginatedList(int page, int pageSize,Company example) {

		CriteriaBuilder builder = getEm().getCriteriaBuilder();

		CriteriaQuery<Company> criteria = builder.createQuery(Company.class);

		Root<Company> root = criteria.from(Company.class);

		root = criteria.from(Company.class);
		TypedQuery<Company> query = getEm().createQuery(criteria.select(root).where(getSearchPredicates(root,example)));
		query.setFirstResult(page * pageSize).setMaxResults(pageSize);
		return query.getResultList();
	}

	private Predicate[] getSearchPredicates(Root<Company> root,Company example) {


		CriteriaBuilder builder = getEm().getCriteriaBuilder();
		List<Predicate> predicatesList = new ArrayList<Predicate>();

		String name = example.getName();
		if (name != null && !"".equals(name)) {
			predicatesList.add(builder.like(builder.lower(root.<String>get("name")), '%' + name.toLowerCase() + '%'));
		}
		String phoneNumber = example.getPhoneNumber();
		if (phoneNumber != null && !"".equals(phoneNumber)) {
			predicatesList.add(builder.like(builder.lower(root.<String>get("phoneNumber")),
					'%' + phoneNumber.toLowerCase() + '%'));
		}
		String address = example.getAddress();
		if (address != null && !"".equals(address)) {
			predicatesList
					.add(builder.like(builder.lower(root.<String>get("address")), '%' + address.toLowerCase() + '%'));
		}
		String email = example.getEmail();
		if (email != null && !"".equals(email)) {
			predicatesList.add(builder.like(builder.lower(root.<String>get("email")), '%' + email.toLowerCase() + '%'));
		}
		String contact = example.getContact();
		if (contact != null && !"".equals(contact)) {
			predicatesList
					.add(builder.like(builder.lower(root.<String>get("contact")), '%' + contact.toLowerCase() + '%'));
		}
		String employees = example.getEmployees();
		if (employees != null && !"".equals(employees)) {
			predicatesList.add(
					builder.like(builder.lower(root.<String>get("employees")), '%' + employees.toLowerCase() + '%'));
		}

		return predicatesList.toArray(new Predicate[predicatesList.size()]);
	}
}