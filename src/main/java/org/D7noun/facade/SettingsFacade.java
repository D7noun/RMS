package org.D7noun.facade;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;

import org.D7noun.model.Settings;

@Stateful
@Local
public class SettingsFacade implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@PersistenceContext(unitName = "ems-pu", type = PersistenceContextType.EXTENDED)
	private EntityManager entityManager;

	public Settings save(Settings settings) {
		return entityManager.merge(settings);
	}

	@SuppressWarnings("unchecked")
	public List<Settings> getAllValues() {
		try {
			Query query = getEntityManager().createNamedQuery(Settings.getAllValues, Settings.class);
			return query.getResultList();
		} catch (Exception e) {
			System.err.println("D7noun: getAllValues");
			e.printStackTrace();
		}
		return null;
	}

	public Settings getSettings(String type) {
		try {
			Query query = getEntityManager().createNamedQuery(Settings.getValues, Settings.class);
			query.setParameter(1, type);
			Settings settings = (Settings) query.getSingleResult();
			return settings;
		} catch (Exception e) {
			System.err.println("D7noun: getSettings");
			e.printStackTrace();
		}
		return null;
	}

	public String getValues(String type) {
		try {
			Query query = getEntityManager().createNamedQuery(Settings.getValues, Settings.class);
			query.setParameter(1, type);
			Settings settings = (Settings) query.getSingleResult();
			return settings.getValue();
		} catch (Exception e) {
			System.err.println("D7noun: getValues");
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 
	 * D7noun
	 * 
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
