package org.D7noun.facade;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.D7noun.model.Unit;

@Stateless
public class UnitFacade extends AbstarctFacade<Unit> {

	public UnitFacade() {
		super(Unit.class);
		// TODO Auto-generated constructor stub
	}

	public UnitFacade(Class<Unit> clazz) {
		super(clazz);
		// TODO Auto-generated constructor stub
	}
	public List<Unit> getAll() {

		CriteriaQuery<Unit> criteria = getEm().getCriteriaBuilder().createQuery(Unit.class);
		return getEm().createQuery(criteria.select(criteria.from(Unit.class))).getResultList();
	}

	public long getCount(Unit example) {

		CriteriaBuilder builder = getEm().getCriteriaBuilder();

		// Populate this.count

		CriteriaQuery<Long> countCriteria = builder.createQuery(Long.class);
		Root<Unit> root = countCriteria.from(Unit.class);
		countCriteria = countCriteria.select(builder.count(root)).where(getSearchPredicates(root,example));
		return getEm().createQuery(countCriteria).getSingleResult();

	}

	public List<Unit> getPaginatedList(int page, int pageSize,Unit example) {

		CriteriaBuilder builder = getEm().getCriteriaBuilder();

		CriteriaQuery<Unit> criteria = builder.createQuery(Unit.class);

		Root<Unit> root = criteria.from(Unit.class);

		root = criteria.from(Unit.class);
		TypedQuery<Unit> query = getEm().createQuery(criteria.select(root).where(getSearchPredicates(root,example)));
		query.setFirstResult(page * pageSize).setMaxResults(pageSize);
		return query.getResultList();
	}

	private Predicate[] getSearchPredicates(Root<Unit> root,Unit example) {

		CriteriaBuilder builder = getEm().getCriteriaBuilder();
		List<Predicate> predicatesList = new ArrayList<Predicate>();

		String uid = example.getUid();
		if (uid != null && !"".equals(uid)) {
			predicatesList.add(builder.like(builder.lower(root.<String>get("uid")), '%' + uid.toLowerCase() + '%'));
		}
		String name =example.getName();
		if (name != null && !"".equals(name)) {
			predicatesList.add(builder.like(builder.lower(root.<String>get("name")), '%' + name.toLowerCase() + '%'));
		}
		String facilityLocation =example.getFacilityLocation();
		if (facilityLocation != null && !"".equals(facilityLocation)) {
			predicatesList.add(builder.like(builder.lower(root.<String>get("facilityLocation")),
					'%' + facilityLocation.toLowerCase() + '%'));
		}

		return predicatesList.toArray(new Predicate[predicatesList.size()]);
	}
}