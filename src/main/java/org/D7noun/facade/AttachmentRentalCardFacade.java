package org.D7noun.facade;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;

import org.D7noun.model.AttachmentRentalCard;
import org.D7noun.model.RentalCard;

@Stateful
@Local
public class AttachmentRentalCardFacade implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@PersistenceContext(unitName = "ems-pu", type = PersistenceContextType.EXTENDED)
	private EntityManager entityManager;

	public AttachmentRentalCard save(AttachmentRentalCard attachment) {
		return entityManager.merge(attachment);
	}

	@SuppressWarnings("unchecked")
	public List<AttachmentRentalCard> findALl() {
		try {
			Query query = getEntityManager().createNamedQuery(AttachmentRentalCard.getAttacmentsByFileId,
					AttachmentRentalCard.class);
			return query.getResultList();
		} catch (Exception e) {
			System.err.println("D7noun: getAllValues");
			e.printStackTrace();
		}
		return null;
	}

	public void removeAttachmentFromTable(RentalCard rentalCard, AttachmentRentalCard deletedAttachment) {
		try {
			deletedAttachment.setRentalCard(null);
			this.entityManager.merge(rentalCard);
			this.entityManager.remove(this.entityManager.contains(deletedAttachment) ? deletedAttachment
					: this.entityManager.merge(deletedAttachment));
			this.entityManager.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * D7noun
	 * 
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
