package org.D7noun.facade;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.D7noun.model.Item;
import org.D7noun.model.Maintainence;


@Stateless
public class MaintenanceFacade extends AbstarctFacade<Maintainence> {

	public MaintenanceFacade() {
		super(Maintainence.class);
		// TODO Auto-generated constructor stub
	}

	public MaintenanceFacade(Class<Maintainence> clazz) {
		super(clazz);
		// TODO Auto-generated constructor stub
	}

	@SuppressWarnings("unchecked")
	public List<Maintainence> getMaintenanceCardsByItem(Item item) {
		try {
			List<Maintainence> result = new ArrayList<>();
			Query query = getEm().createNamedQuery(Maintainence.getMaintenanceCardsByItem, Maintainence.class);
			query.setParameter("item", item);
			result = query.getResultList();
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: MaintenanceBean.getMaintenanceCardsByItem");
		}
		return new ArrayList<>();
	}

	public void deleteMaintenanceCardsByItem(Item item) {
		try {
			Query query = getEm().createNamedQuery(Maintainence.deleteMaintenanceCardsByItem, Maintainence.class);
			query.setParameter("item", item);
			query.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("MaintenanceBean.deleteMaintenanceCardsByItem");
		}
	}

	public List<Maintainence> getAll() {

		CriteriaQuery<Maintainence> criteria = getEm().getCriteriaBuilder().createQuery(Maintainence.class);
		return getEm().createQuery(criteria.select(criteria.from(Maintainence.class))).getResultList();
	}

	public long getCount(Maintainence example) {

		CriteriaBuilder builder = getEm().getCriteriaBuilder();

		// Populate this.count

		CriteriaQuery<Long> countCriteria = builder.createQuery(Long.class);
		Root<Maintainence> root = countCriteria.from(Maintainence.class);
		countCriteria = countCriteria.select(builder.count(root)).where(getSearchPredicates(root, example));
		return getEm().createQuery(countCriteria).getSingleResult();

	}

	public List<Maintainence> getPaginatedList(int page, int pageSize, Maintainence example) {

		CriteriaBuilder builder = getEm().getCriteriaBuilder();

		CriteriaQuery<Maintainence> criteria = builder.createQuery(Maintainence.class);

		Root<Maintainence> root = criteria.from(Maintainence.class);

		root = criteria.from(Maintainence.class);
		TypedQuery<Maintainence> query = getEm()
				.createQuery(criteria.select(root).where(getSearchPredicates(root, example)));
		query.setFirstResult(page * pageSize).setMaxResults(pageSize);
		return query.getResultList();
	}

	private Predicate[] getSearchPredicates(Root<Maintainence> root, Maintainence example) {

		CriteriaBuilder builder = getEm().getCriteriaBuilder();
		List<Predicate> predicatesList = new ArrayList<Predicate>();

		Item item = example.getItem();
		Date startDate = example.getStartDate();
		Date endDate = example.getEndDate();
		Date dueDate = example.getDueDate();
		String malfunctioning = example.getMalfunctioning();
		Double fees = example.getFees();
		String maintenanceMan = example.getMaintenanceMan();

		if (item != null) {
			predicatesList.add(builder.equal(root.get("item"), item));
		}

		if (startDate != null) {
			predicatesList.add(builder.equal(root.get("startDate"), startDate));
		}

		if (endDate != null) {
			predicatesList.add(builder.equal(root.get("endDate"), endDate));
		}

		if (dueDate != null) {
			predicatesList.add(builder.equal(root.get("dueDate"), dueDate));
		}

		if (malfunctioning != null && !"".equals(malfunctioning)) {
			predicatesList.add(builder.like(builder.lower(root.<String>get("malfunctioning")),
					'%' + malfunctioning.toLowerCase() + '%'));
		}

		if (fees != null) {
			predicatesList.add(builder.equal(root.get("fees"), fees));
		}

		if (maintenanceMan != null && !"".equals(maintenanceMan)) {
			predicatesList.add(builder.like(builder.lower(root.<String>get("maintenanceMan")),
					'%' + maintenanceMan.toLowerCase() + '%'));
		}

		return predicatesList.toArray(new Predicate[predicatesList.size()]);
	}

}