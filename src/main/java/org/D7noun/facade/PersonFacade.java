package org.D7noun.facade;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.D7noun.model.Person;
import org.D7noun.model.Unit;

@Stateless
public class PersonFacade extends AbstarctFacade<Person> {

	public PersonFacade() {
		super(Person.class);
		// TODO Auto-generated constructor stub
	}

	public PersonFacade(Class<Person> clazz) {
		super(clazz);
		// TODO Auto-generated constructor stub
	}

	public List<Person> getAll() {

		CriteriaQuery<Person> criteria = getEm().getCriteriaBuilder().createQuery(Person.class);
		return getEm().createQuery(criteria.select(criteria.from(Person.class))).getResultList();
	}

	public long getCount(Person example) {

		CriteriaBuilder builder = getEm().getCriteriaBuilder();

		// Populate this.count

		CriteriaQuery<Long> countCriteria = builder.createQuery(Long.class);
		Root<Person> root = countCriteria.from(Person.class);
		countCriteria = countCriteria.select(builder.count(root)).where(getSearchPredicates(root,example));
		return getEm().createQuery(countCriteria).getSingleResult();

	}

	public List<Person> getPaginatedList(int page, int pageSize,Person example) {

		CriteriaBuilder builder = getEm().getCriteriaBuilder();

		CriteriaQuery<Person> criteria = builder.createQuery(Person.class);

		Root<Person> root = criteria.from(Person.class);

		root = criteria.from(Person.class);
		TypedQuery<Person> query = getEm().createQuery(criteria.select(root).where(getSearchPredicates(root,example)));
		query.setFirstResult(page * pageSize).setMaxResults(pageSize);
		return query.getResultList();
	}

	private Predicate[] getSearchPredicates(Root<Person> root,Person example) {

		CriteriaBuilder builder = getEm().getCriteriaBuilder();
		List<Predicate> predicatesList = new ArrayList<Predicate>();

		String pid =example.getPid();
		String name = example.getName();
		String facilityLocation = example.getFacilityLocation();
		Unit unit = example.getUnit();

		if (pid != null && !"".equals(pid)) {
			predicatesList.add(builder.like(builder.lower(root.<String>get("pid")), '%' + pid.toLowerCase() + '%'));
		}

		if (name != null && !"".equals(name)) {
			predicatesList.add(builder.like(builder.lower(root.<String>get("name")), '%' + name.toLowerCase() + '%'));
		}

		if (facilityLocation != null && !"".equals(facilityLocation)) {
			predicatesList.add(builder.like(builder.lower(root.<String>get("facilityLocation")),
					'%' + facilityLocation.toLowerCase() + '%'));
		}

		if (unit != null) {
			predicatesList.add(builder.equal(root.get("unit"), unit));
		}

		return predicatesList.toArray(new Predicate[predicatesList.size()]);
	}

}
