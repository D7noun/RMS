package org.D7noun.facade;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;

import org.D7noun.model.AttachmentItem;
import org.D7noun.model.Item;

@Stateful
@Local
public class AttachmentItemFacade implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@PersistenceContext(unitName = "ems-pu", type = PersistenceContextType.EXTENDED)
	private EntityManager entityManager;

	public AttachmentItem save(AttachmentItem attachment) {
		return entityManager.merge(attachment);
	}

	public AttachmentItem findById(long attachmentItemId) {
		try {
			Query query = getEntityManager().createNamedQuery(AttachmentItem.findById, AttachmentItem.class);
			query.setParameter(1, attachmentItemId);
			return (AttachmentItem) query.getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: getAttachmentItemDataFromAttachmentItemId");
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public List<AttachmentItem> findALl() {
		try {
			Query query = getEntityManager().createNamedQuery(AttachmentItem.getAttacmentsByFileId,
					AttachmentItem.class);
			return query.getResultList();
		} catch (Exception e) {
			System.err.println("D7noun: getAllValues");
			e.printStackTrace();
		}
		return null;
	}

	public void removeAttachmentFromTable(Item item, AttachmentItem deletedAttachment) {
		try {
			this.entityManager.merge(item);
			this.entityManager.remove(this.entityManager.contains(deletedAttachment) ? deletedAttachment
					: this.entityManager.merge(deletedAttachment));
			this.entityManager.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * D7noun
	 * 
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
