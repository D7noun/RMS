package org.D7noun.facade;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Session Bean implementation class AbstarctFacade
 */
@Stateless
@LocalBean
public  class AbstarctFacade<T> {

	@PersistenceContext(unitName = "ems-pu")
	private EntityManager em;

	private Class<T> clazz;

	public AbstarctFacade() {

	}

	public AbstarctFacade(Class<T> clazz) {

		this.clazz = clazz;

	}

	public T save(T entity) {

		return em.merge(entity);
	}

	public T find(Long id) {

		return em.find(clazz, id);

	}

	public EntityManager getEm() {
		return em;
	}

	public void setEm(EntityManager em) {
		this.em = em;
	}

	public void remove(T entity) {

		em.remove(em.merge(entity));
	}

}
