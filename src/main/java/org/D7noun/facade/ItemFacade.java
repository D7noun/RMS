package org.D7noun.facade;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.D7noun.model.Damaged;
import org.D7noun.model.Item;
import org.D7noun.model.Item.State;
import org.D7noun.model.Maintainence;
import org.D7noun.model.RentalCard;

@Stateless
public class ItemFacade extends AbstarctFacade<Item> {

	

	@EJB
	private MaintenanceFacade maintainenceBean;
	@EJB
	private RentalCardFacade rentalCardBean;
	@EJB
	private DamageFacade damagedBean;
	public ItemFacade() {
		super(Item.class);
		// TODO Auto-generated constructor stub
	}

	public ItemFacade(Class<Item> clazz) {
		super(clazz);
		// TODO Auto-generated constructor stub
	}

	public List<Item> getAll() {

		CriteriaQuery<Item> criteria = getEm().getCriteriaBuilder().createQuery(Item.class);
		return getEm().createQuery(criteria.select(criteria.from(Item.class))).getResultList();
	}

	public Item findBySerialNumber(String serialNumber) {
		try {
			Query query = getEm().createNamedQuery(Item.findItemBySerialNumber, Item.class);
			query.setParameter(1, serialNumber);
			return (Item) query.getSingleResult();
		} catch (Exception e) {
			System.err.println("EXCEPTION: findBySerialNumber");
			e.printStackTrace();
		}
		return null;

	}

	public void delete(Item deletableEntity) {

		Iterator<Maintainence> iterMaintenances = maintainenceBean.getMaintenanceCardsByItem(deletableEntity)
				.iterator();
		for (; iterMaintenances.hasNext();) {
			Maintainence next = iterMaintenances.next();
			next.setItem(null);
			iterMaintenances.remove();
			maintainenceBean.remove(next);
		}
		Iterator<RentalCard> iterRentalCards = rentalCardBean.getRentalCardsByItem(deletableEntity).iterator();
		for (; iterRentalCards.hasNext();) {
			RentalCard next = iterRentalCards.next();
			next.setItem(null);
			iterRentalCards.remove();
			rentalCardBean.remove(next);
		}
		Iterator<Damaged> iterDamages = damagedBean.getDamagedCardsByItem(deletableEntity).iterator();
		for (; iterDamages.hasNext();) {
			Damaged next = iterDamages.next();
			next.setItem(null);
			iterDamages.remove();
			damagedBean.remove(next);
		}

		getEm().remove(deletableEntity);

	}

	@SuppressWarnings("unchecked")
	public List<Item> getAllAvailableItems(Long id) {
		try {
			Query query = getEm().createNamedQuery(Item.allAvailableAccessories, Item.class);
			query.setParameter(1, State.available);
			query.setParameter(2, id);
			return query.getResultList();
		} catch (Exception e) {
			System.err.println("D7noun: getAllAvailableItems");
			e.printStackTrace();
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public List<Item> getAccessoriesByItemSerialNumber(String serialNumber) {
		try {
			Query query = getEm().createNamedQuery(Item.findAccessoriesByItemSerialNumber, Item.class);
			query.setParameter(1, serialNumber);
			return query.getResultList();
		} catch (Exception e) {
			System.err.println("D7noun:getAccessoriesByItemSerialNumber");
			e.printStackTrace();
		}
		return null;
	}

	public long getCount(Item example) {

		CriteriaBuilder builder = getEm().getCriteriaBuilder();

		// Populate this.count

		CriteriaQuery<Long> countCriteria = builder.createQuery(Long.class);
		Root<Item> root = countCriteria.from(Item.class);
		countCriteria = countCriteria.select(builder.count(root)).where(getSearchPredicates(root, example));
		return getEm().createQuery(countCriteria).getSingleResult();

	}

	public List<Item> getPaginatedList(int page, int pageSize, Item example) {

		CriteriaBuilder builder = getEm().getCriteriaBuilder();

		CriteriaQuery<Item> criteria = builder.createQuery(Item.class);

		Root<Item> root = criteria.from(Item.class);

		root = criteria.from(Item.class);
		TypedQuery<Item> query = getEm().createQuery(criteria.select(root).where(getSearchPredicates(root, example)));
		query.setFirstResult(page * pageSize).setMaxResults(pageSize);
		return query.getResultList();
	}

	private Predicate[] getSearchPredicates(Root<Item> root, Item example) {

		CriteriaBuilder builder = getEm().getCriteriaBuilder();
		List<Predicate> predicatesList = new ArrayList<Predicate>();

		String deviceId = example.getDeviceId();
		if (deviceId != null && !"".equals(deviceId)) {
			predicatesList
					.add(builder.like(builder.lower(root.<String>get("deviceId")), '%' + deviceId.toLowerCase() + '%'));
		}
		String name = example.getName();
		if (name != null && !"".equals(name)) {
			predicatesList.add(builder.like(builder.lower(root.<String>get("name")), '%' + name.toLowerCase() + '%'));
		}
		String serialNumber = example.getSerialNumber();
		if (serialNumber != null && !"".equals(serialNumber)) {
			predicatesList.add(builder.like(builder.lower(root.<String>get("serialNumber")),
					'%' + serialNumber.toLowerCase() + '%'));
		}
		String model = example.getModel();
		if (model != null && !"".equals(model)) {
			predicatesList.add(builder.like(builder.lower(root.<String>get("model")), '%' + model.toLowerCase() + '%'));
		}
		String origin = example.getOrigin();
		if (origin != null && !"".equals(origin)) {
			predicatesList
					.add(builder.like(builder.lower(root.<String>get("origin")), '%' + origin.toLowerCase() + '%'));
		}

		return predicatesList.toArray(new Predicate[predicatesList.size()]);
	}
}