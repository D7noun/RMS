package org.D7noun.facade;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.D7noun.model.Item;
import org.D7noun.model.Person;
import org.D7noun.model.RentalCard;

@Stateless
public class RentalCardFacade extends AbstarctFacade<RentalCard> {

	public RentalCardFacade() {
		super(RentalCard.class);
		// TODO Auto-generated constructor stub
	}

	public RentalCardFacade(Class<RentalCard> clazz) {
		super(clazz);
		// TODO Auto-generated constructor stub
	}

	@SuppressWarnings("unchecked")
	public List<RentalCard> getRentalCardsByItem(Item item) {
		try {
			List<RentalCard> result = new ArrayList<>();
			Query query = getEm().createNamedQuery(RentalCard.getRentalCardsByItem, RentalCard.class);
			query.setParameter("item", item);
			result = query.getResultList();
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: RentalCardBean.getRentalsCardsByItem");
		}
		return new ArrayList<>();
	}

	public void deleteRentalCardsByItem(Item item) {
		try {
			Query query = getEm().createNamedQuery(RentalCard.deleteRentalCardsByItem, RentalCard.class);
			query.setParameter("item", item);
			query.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("RentalCardBean.deleteRentalCardsByItem");
		}
	}
	public List<RentalCard> getAll() {

		CriteriaQuery<RentalCard> criteria = getEm().getCriteriaBuilder().createQuery(RentalCard.class);
		return getEm().createQuery(criteria.select(criteria.from(RentalCard.class))).getResultList();
	}

	public long getCount(RentalCard example) {

		CriteriaBuilder builder = getEm().getCriteriaBuilder();

		// Populate this.count

		CriteriaQuery<Long> countCriteria = builder.createQuery(Long.class);
		Root<RentalCard> root = countCriteria.from(RentalCard.class);
		countCriteria = countCriteria.select(builder.count(root)).where(getSearchPredicates(root,example));
		return getEm().createQuery(countCriteria).getSingleResult();

	}

	public List<RentalCard> getPaginatedList(int page, int pageSize,RentalCard example) {

		CriteriaBuilder builder = getEm().getCriteriaBuilder();

		CriteriaQuery<RentalCard> criteria = builder.createQuery(RentalCard.class);

		Root<RentalCard> root = criteria.from(RentalCard.class);

		root = criteria.from(RentalCard.class);
		TypedQuery<RentalCard> query = getEm().createQuery(criteria.select(root).where(getSearchPredicates(root,example)));
		query.setFirstResult(page * pageSize).setMaxResults(pageSize);
		return query.getResultList();
	}

	private Predicate[] getSearchPredicates(Root<RentalCard> root,RentalCard example) {

		CriteriaBuilder builder = getEm().getCriteriaBuilder();
		List<Predicate> predicatesList = new ArrayList<Predicate>();

		Date startDate = example.getStartDate();
		Date endDate = example.getEndDate();
		Date dueDate = example.getDueDate();
		Person user = example.getUser();
		Item item = example.getItem();

		if (startDate != null) {
			predicatesList.add(builder.equal(root.get("startDate"), startDate));
		}

		if (endDate != null) {
			predicatesList.add(builder.equal(root.get("endDate"), endDate));
		}

		if (dueDate != null) {
			predicatesList.add(builder.equal(root.get("dueDate"), dueDate));
		}

		if (user != null) {
			predicatesList.add(builder.equal(root.get("user"), user));
		}

		if (item != null) {
			predicatesList.add(builder.equal(root.get("item"), item));
		}

		return predicatesList.toArray(new Predicate[predicatesList.size()]);
	}

}