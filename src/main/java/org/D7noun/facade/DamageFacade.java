package org.D7noun.facade;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.D7noun.model.Damaged;
import org.D7noun.model.Item;
import org.D7noun.model.Maintainence;
import org.D7noun.model.Person;

@Stateless
public class DamageFacade extends AbstarctFacade<Damaged> {

	public DamageFacade() {
		super(Damaged.class);
		// TODO Auto-generated constructor stub
	}

	public DamageFacade(Class<Damaged> clazz) {
		super(clazz);
		// TODO Auto-generated constructor stub
	}

	public List<Damaged> getAll() {

		CriteriaQuery<Damaged> criteria = getEm().getCriteriaBuilder().createQuery(Damaged.class);
		return getEm().createQuery(criteria.select(criteria.from(Damaged.class))).getResultList();
	}

	public long getCount(Damaged example) {

		CriteriaBuilder builder = getEm().getCriteriaBuilder();

		// Populate this.count

		CriteriaQuery<Long> countCriteria = builder.createQuery(Long.class);
		Root<Damaged> root = countCriteria.from(Damaged.class);
		countCriteria = countCriteria.select(builder.count(root)).where(getSearchPredicates(root, example));
		return getEm().createQuery(countCriteria).getSingleResult();

	}

	public List<Damaged> getPaginatedList(int page, int pageSize, Damaged example) {

		CriteriaBuilder builder = getEm().getCriteriaBuilder();

		CriteriaQuery<Damaged> criteria = builder.createQuery(Damaged.class);

		Root<Damaged> root = criteria.from(Damaged.class);

		root = criteria.from(Damaged.class);
		TypedQuery<Damaged> query = getEm()
				.createQuery(criteria.select(root).where(getSearchPredicates(root, example)));
		query.setFirstResult(page * pageSize).setMaxResults(pageSize);
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Damaged> getDamagedCardsByItem(Item item) {
		try {
			List<Damaged> result = new ArrayList<>();
			Query query = getEm().createNamedQuery(Damaged.getDamagedCardsByItemId, Damaged.class);
			query.setParameter("item", item);
			result = query.getResultList();
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: DamagedBean.getDamagedCardsByItem");
		}
		return new ArrayList<>();
	}

	private Predicate[] getSearchPredicates(Root<Damaged> root, Damaged example) {

		CriteriaBuilder builder = getEm().getCriteriaBuilder();
		List<Predicate> predicatesList = new ArrayList<Predicate>();

		Date date = example.getDate();
		String reason = example.getReason();
		Item item = example.getItem();
		Person personInCharge = example.getPersonInCharge();

		if (date != null) {
			predicatesList.add(builder.equal(root.get("date"), date));
		}

		if (reason != null && !"".equals(reason)) {
			predicatesList
					.add(builder.like(builder.lower(root.<String>get("reason")), '%' + reason.toLowerCase() + '%'));
		}

		if (item != null) {
			predicatesList.add(builder.equal(root.get("item"), item));
		}

		if (personInCharge != null) {
			predicatesList.add(builder.equal(root.get("personInCharge"), personInCharge));
		}

		return predicatesList.toArray(new Predicate[predicatesList.size()]);
	}

	public void deleteDamageddCardsByItem(Item item) {
		try {
			Query query = getEm().createNamedQuery(Maintainence.deleteMaintenanceCardsByItem, Maintainence.class);
			query.setParameter("item", item);
			query.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("MaintenanceBean.deleteMaintenanceCardsByItem");
		}
	}

	@SuppressWarnings("unchecked")
	public List<Damaged> getDamageddCardsByItem(Item item) {
		try {
			List<Damaged> result = new ArrayList<>();
			Query query = getEm().createNamedQuery(Damaged.getDamagedCardsByItemId, Damaged.class);
			query.setParameter("item", item);
			result = query.getResultList();
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: DamageddBean.getDamageddCardsByItem");
		}
		return new ArrayList<>();
	}
}