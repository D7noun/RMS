package org.D7noun.controller;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.D7noun.facade.SettingsFacade;
import org.D7noun.model.Settings;

@ManagedBean
@ViewScoped
public class SettingsController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	private SettingsFacade settingsFacade;

	private Settings specilities;
	private Settings usages;
	private Settings domains;
	private Settings types;

	@PostConstruct
	public void init() {
		specilities = settingsFacade.getSettings("specilities");
		usages = settingsFacade.getSettings("usages");
		domains = settingsFacade.getSettings("domains");
		types = settingsFacade.getSettings("types");

	}

	public void save() {
		try {
			specilities = settingsFacade.save(specilities);
			usages = settingsFacade.save(usages);
			domains = settingsFacade.save(domains);
			types = settingsFacade.save(types);
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage("Save Succeeded"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * D7noun
	 * 
	 */

	public SettingsFacade getSettingsFacade() {
		return settingsFacade;
	}

	public void setSettingsFacade(SettingsFacade settingsFacade) {
		this.settingsFacade = settingsFacade;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the specilities
	 */
	public Settings getSpecilities() {
		return specilities;
	}

	/**
	 * @param specilities
	 *            the specilities to set
	 */
	public void setSpecilities(Settings specilities) {
		this.specilities = specilities;
	}

	/**
	 * @return the usages
	 */
	public Settings getUsages() {
		return usages;
	}

	/**
	 * @param usages
	 *            the usages to set
	 */
	public void setUsages(Settings usages) {
		this.usages = usages;
	}

	/**
	 * @return the types
	 */
	public Settings getTypes() {
		return types;
	}

	/**
	 * @param types
	 *            the types to set
	 */
	public void setTypes(Settings types) {
		this.types = types;
	}

	/**
	 * @return the domains
	 */
	public Settings getDomains() {
		return domains;
	}

	/**
	 * @param domains
	 *            the domains to set
	 */
	public void setDomains(Settings domains) {
		this.domains = domains;
	}

}