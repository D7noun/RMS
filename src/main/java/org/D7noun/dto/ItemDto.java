package org.D7noun.dto;

public class ItemDto {

	private String deviceId;
	private String name;
	private String model;
	private String serialNumber;
	private String manufacturer;

	public ItemDto(String deviceId, String name, String model, String serialNumber, String manufacturer) {
		super();
		this.deviceId = deviceId;
		this.name = name;
		this.model = model;
		this.serialNumber = serialNumber;
		this.manufacturer = manufacturer;
	}

	public ItemDto(String deviceId, String name, String model, String serialNumber) {
		super();
		this.deviceId = deviceId;
		this.name = name;
		this.model = model;
		this.serialNumber = serialNumber;
	}

	public ItemDto() {
		super();
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the deviceId
	 */
	public String getDeviceId() {
		return deviceId;
	}

	/**
	 * @param deviceId
	 *            the deviceId to set
	 */
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	/**
	 * @return the model
	 */
	public String getModel() {
		return model;
	}

	/**
	 * @param model
	 *            the model to set
	 */
	public void setModel(String model) {
		this.model = model;
	}

	/**
	 * @return the manufacturer
	 */
	public String getManufacturer() {
		return manufacturer;
	}

	/**
	 * @param manufacturer
	 *            the manufacturer to set
	 */
	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

}
