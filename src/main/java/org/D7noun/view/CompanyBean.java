package org.D7noun.view;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Named;

import org.D7noun.facade.CompanyFacade;
import org.D7noun.model.Company;

@Named
@SessionScoped
public class CompanyBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private CompanyFacade companyFacade;

	private Long id;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	private Company manufacturer;

	public Company getManufacturer() {
		return this.manufacturer;
	}

	public void setManufacturer(Company manufacturer) {
		this.manufacturer = manufacturer;
	}

	public String create() {

		this.id = null;

		return "create?faces-redirect=true";
	}

	public void retrieve() {

		if (FacesContext.getCurrentInstance().isPostback()) {
			return;
		}

		if (this.id == null) {
			this.manufacturer = this.example;
		} else {
			this.manufacturer = findById(getId());
		}
	}

	public Company findById(Long id) {

		return companyFacade.find(id);
	}

	/*
	 * Support updating and deleting Manufacturer entities
	 */

	public String update() {

		try {
			companyFacade.save(this.manufacturer);
			return "search?faces-redirect=true";
		} catch (Exception e) {

			if (e.getCause() != null && e.getCause().getCause() != null && e.getCause().getCause().getCause() != null) {

				if (e.getCause().getCause().getCause().getMessage()
						.contains("duplicate key value violates unique constraint"))
					FacesContext.getCurrentInstance().addMessage(null,
							new FacesMessage("there is already a Company with this name"));
				else
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getMessage()));
			} else {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getMessage()));

			}
			return null;
		}
	}

	public String delete() {

		try {
			Company deletableEntity = findById(getId());

			companyFacade.remove(deletableEntity);
			return "search?faces-redirect=true";
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getMessage()));
			return null;
		}
	}

	/*
	 * Support searching Manufacturer entities with pagination
	 */

	private int page;
	private long count;
	private List<Company> pageItems;

	private Company example = new Company();

	public int getPage() {
		return this.page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getPageSize() {
		return 10;
	}

	public Company getExample() {
		return this.example;
	}

	public void setExample(Company example) {
		this.example = example;
	}

	public String search() {
		this.page = 0;
		return null;
	}

	public String clearSearch() {
		this.example = new Company();
		return null;
	}

	public void paginate() {

		this.count = companyFacade.getCount(this.example);

		this.pageItems = companyFacade.getPaginatedList(this.page, getPageSize(), this.example);
	}

	public List<Company> getPageItems() {
		return this.pageItems;
	}

	public long getCount() {
		return this.count;
	}

	/*
	 * Support listing and POSTing back Manufacturer entities (e.g. from inside
	 * an HtmlSelectOneMenu)
	 */

	public List<Company> getAll() {

		return companyFacade.getAll();
	}

	public Converter getConverter() {

		return new Converter() {

			@Override
			public Object getAsObject(FacesContext context, UIComponent component, String value) {

				return companyFacade.find(Long.valueOf(value));
			}

			@Override
			public String getAsString(FacesContext context, UIComponent component, Object value) {

				if (value == null) {
					return "";
				}

				return String.valueOf(((Company) value).getId());
			}
		};
	}

	/*
	 * Support adding children to bidirectional, one-to-many tables
	 */

	private Company add = new Company();

	public Company getAdd() {
		return this.add;
	}

	public Company getAdded() {
		Company added = this.add;
		this.add = new Company();
		return added;
	}
}
