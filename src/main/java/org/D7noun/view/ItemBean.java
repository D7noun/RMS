package org.D7noun.view;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.event.PhaseId;
import javax.inject.Named;
import javax.print.DocFlavor.STRING;

import org.D7noun.facade.AttachmentItemFacade;
import org.D7noun.facade.ItemFacade;
import org.D7noun.model.AttachmentItem;
import org.D7noun.model.Item;
import org.D7noun.model.Unit;
import org.D7noun.model.Item.State;
import org.omnifaces.util.Ajax;
import org.omnifaces.util.Faces;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

/**
 * Backing bean for Item entities.
 * <p/>
 * This class provides CRUD functionality for all Item entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD
 * framework or custom base class.
 */

@Named
@SessionScoped
public class ItemBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private ItemFacade itemFacade;

	private Long id;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	private Item item;

	public Item getItem() {
		return this.item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public String create() {

		this.id = null;

		return "create?faces-redirect=true";
	}

	public void retrieve() {

		if (FacesContext.getCurrentInstance().isPostback()) {
			return;
		}

		if (this.id == null) {
			this.item = this.example;
		} else {
			this.item = findById(getId());

		}

	}

	public String clearSearch() {
		this.example = new Item();
		return null;
	}

	public Item findById(Long id) {

		return itemFacade.find(id);

	}

	public Item findBySerialNumber(String serialNumber) {

		return itemFacade.findBySerialNumber(serialNumber);
	}

	/*
	 * Support updating and deleting Item entities
	 */
	public boolean validString(String s) {

		if (s == null || s.isEmpty() || s.trim().isEmpty()) {

			return false;
		}
		return true;
	}

	public String update() {

		if (accessories != null && !accessories.isEmpty()) {
			for (Item accessory : convertsIdsIntoItems(accessories)) {
				this.item.addAccessory(accessory);
			}
		}
		try {
			this.item = itemFacade.save(this.item);
			this.example = new Item();
			return "search?faces-redirect=true";
		} catch (Exception e) {
			if (e.getCause() != null && e.getCause().getCause() != null && e.getCause().getCause().getCause() != null) {

				if (e.getCause().getCause().getCause().getMessage().contains("item_deviceid_key"))
					FacesContext.getCurrentInstance().addMessage(null,
							new FacesMessage("there is already an item with this deviceId"));
				else if (e.getCause().getCause().getCause().getMessage().contains("item_name_key"))
					FacesContext.getCurrentInstance().addMessage(null,
							new FacesMessage("there is already an item with this name"));
				else
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getMessage()));
			} else {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getMessage()));
			}
			return null;
		}
	}

	public String delete() {

		try {
			Item deletableEntity = findById(getId());

			if (deletableEntity.getState() == State.damaged) {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Is Damaged"));
				return null;
			}
			if (deletableEntity.getState() == State.rented) {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Is Rented"));
				return null;
			}
			if (deletableEntity.getState() == State.under_maintenance) {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Under Maintenance"));
				return null;
			}
			itemFacade.remove(deletableEntity);

			return "search?faces-redirect=true";
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/*
	 * Support searching Item entities with pagination
	 */

	private int page;
	private long count;
	private List<Item> pageItems;

	private Item example = new Item();

	public int getPage() {
		return this.page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getPageSize() {
		return 10;
	}

	public Item getExample() {
		return this.example;
	}

	public void setExample(Item example) {
		this.example = example;
	}

	public String search() {
		this.page = 0;
		return null;
	}

	public void paginate() {

		this.count = itemFacade.getCount(this.example);

		this.pageItems = itemFacade.getPaginatedList(this.page, getPageSize(), this.example);

	}

	public List<Item> getPageItems() {
		return this.pageItems;
	}

	public long getCount() {
		return this.count;
	}

	public List<Item> getAll() {

		return itemFacade.getAll();
	}

	public Converter getConverter() {

		return new Converter() {

			@Override
			public Object getAsObject(FacesContext context, UIComponent component, String value) {

				return itemFacade.find(Long.valueOf(value));
			}

			@Override
			public String getAsString(FacesContext context, UIComponent component, Object value) {

				if (value == null) {
					return "";
				}

				return String.valueOf(((Item) value).getId());
			}
		};
	}

	/*
	 * Support adding children to bidirectional, one-to-many tables
	 */

	private Item add = new Item();

	public Item getAdd() {
		return this.add;
	}

	public Item getAdded() {
		Item added = this.add;
		this.add = new Item();
		return added;
	}

	/**
	 * D7noun
	 *********************************************************************/

	List<String> accessories = new ArrayList<>();

	public List<Item> getAllAvailableItems() {
		return itemFacade.getAllAvailableItems(this.id);
	}

	public List<Item> getAccessoriesByItemSerialNumber(String serialNumber) {
		return itemFacade.getAccessoriesByItemSerialNumber(serialNumber);
	}

	public List<Item> convertsIdsIntoItems(List<String> ids) {
		List<Item> items = new ArrayList<Item>();
		for (String id : ids) {
			items.add(findBySerialNumber(id));
		}
		return items;
	}

	public void handleFileUpload(FileUploadEvent event) {
		try {

			UploadedFile uploadedFile = event.getFile();

			AttachmentItem attachment = new AttachmentItem();
			attachment.setFileName(uploadedFile.getFileName());
			attachment.setSize(uploadedFile.getSize());
			attachment.setContentType(uploadedFile.getContentType());
			attachment.setData(uploadedFile.getContents());
			item.getAttachments().add(attachment);
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("ItemBean: handleFileUpload");
		}
	}

	public void download(AttachmentItem attachment) {

		try {
			Faces.sendFile(attachment.getData(), attachment.getFileName(), true);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@EJB
	private AttachmentItemFacade attachmentFacade;
	private AttachmentItem attachmentFordelete;

	public void selectForDelete(AttachmentItem attachment) {
		attachmentFordelete = attachment;
	}

	public void deleteAttachment() {
		try {
			item.getAttachments().remove(attachmentFordelete);
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}

	public StreamedContent getPicture() {
		FacesContext context = FacesContext.getCurrentInstance();

		if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
			return new DefaultStreamedContent();
		} else {
			String attachmentId = context.getExternalContext().getRequestParameterMap().get("attachmentId");
			if (attachmentId != null) {
				AttachmentItem attachmentItem = attachmentFacade.findById(Long.parseLong(attachmentId));
				StreamedContent streamedContent = new DefaultStreamedContent(
						new ByteArrayInputStream(attachmentItem.getData()));
				return streamedContent;
			}
		}
		return new DefaultStreamedContent();
	}

	/**
	 * @return the accessories
	 */
	public List<String> getAccessories() {
		return accessories;
	}

	/**
	 * @param accessories
	 *            the accessories to set
	 */
	public void setAccessories(List<String> accessories) {
		this.accessories = accessories;
	}

}