package org.D7noun.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Named;

import org.D7noun.facade.PersonFacade;
import org.D7noun.facade.RentalCardFacade;
import org.D7noun.facade.UnitFacade;
import org.D7noun.model.Person;
import org.D7noun.model.RentalCard;
import org.D7noun.model.Unit;
import org.D7noun.util.CommonUtility;

@Named
@SessionScoped
public class PersonBean implements Serializable {

	private static final long serialVersionUID = 1L;

	/*
	 * Support creating and retrieving Person entities
	 */

	@EJB
	private RentalCardFacade rentalCardFacade;
	@EJB
	private PersonFacade personFacade;
	@EJB
	private UnitFacade unitFacade;
	private Long id;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	private Person person;

	public Person getPerson() {
		return this.person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public String create() {

		this.id = null;
		return "create?faces-redirect=true";
	}

	public void retrieve() {

		if (FacesContext.getCurrentInstance().isPostback()) {
			return;
		}

		if (this.id == null) {
			this.person = this.example;
		} else {
			this.person = findById(getId());
			fillDepartmentsByUnit();
		}
	}
	public String clearSearch() {
		this.example=new Person();
		return null;
	}
	public Person findById(Long id) {

		return personFacade.find(id);
	}

	/*
	 * Support updating and deleting Person entities
	 */

	public String update() {
		// this.conversation.end();

		try {
			if (this.person.getUnit() == null)
				throw new Exception("The unit is required");
			personFacade.save(this.person);
			this.example = new Person();
			return "search?faces-redirect=true";

		} catch (Exception e) {

			if (e.getCause() != null && e.getCause().getCause() != null && e.getCause().getCause().getCause() != null) {

				if (e.getCause().getCause().getCause().getMessage()
						.contains("duplicate key value violates unique constraint"))
					FacesContext.getCurrentInstance().addMessage(null,
							new FacesMessage("there is already a person with this pid"));
				else
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getMessage()));
			} else {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getMessage()));

			}
			return null;
		}
	}

	public String delete() {

		try {
			Person deletableEntity = findById(getId());
			Unit unit = deletableEntity.getUnit();
			if (unit != null) {
				unit.getPersons().remove(deletableEntity);
				deletableEntity.setUnit(null);
				unitFacade.save(unit);
			}

			RentalCard rentalCard = deletableEntity.getRentalCard();
			if (rentalCard != null) {
				rentalCard.setUser(null);
				rentalCardFacade.save(rentalCard);
			}
			personFacade.remove(deletableEntity);
			// personFacade.
			return "search?faces-redirect=true";
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getMessage()));
			return null;
		}
	}

	/*
	 * Support searching Person entities with pagination
	 */

	private int page;
	private long count;
	private List<Person> pageItems;

	private Person example = new Person();

	public int getPage() {
		return this.page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getPageSize() {
		return 10;
	}

	public Person getExample() {
		return this.example;
	}

	public void setExample(Person example) {
		this.example = example;
	}

	public String search() {
		this.page = 0;
		return null;
	}

	public void paginate() {

		this.count = personFacade.getCount(this.example);

		this.pageItems = personFacade.getPaginatedList(this.page, getPageSize(), this.example);

	}

	public List<Person> getPageItems() {
		return this.pageItems;
	}

	public long getCount() {
		return this.count;
	}

	public List<Person> getAll() {

		return personFacade.getAll();
	}

	public Converter getConverter() {

		return new Converter() {

			@Override
			public Object getAsObject(FacesContext context, UIComponent component, String value) {

				return personFacade.find(Long.valueOf(value));
			}

			@Override
			public String getAsString(FacesContext context, UIComponent component, Object value) {

				if (value == null) {
					return "";
				}

				return String.valueOf(((Person) value).getId());
			}
		};
	}

	/*
	 * Support adding children to bidirectional, one-to-many tables
	 */

	private Person add = new Person();

	public Person getAdd() {
		return this.add;
	}

	public Person getAdded() {
		Person added = this.add;
		this.add = new Person();
		return added;
	}

	/**
	 * 
	 * D7noun
	 * 
	 */

	private List<String> departments = new ArrayList<String>();
	private List<String> facilityLocations = new ArrayList<String>();

	public void fillDepartmentsByUnit() {
		if (person != null && person.getUnit() != null) {
			departments=new  ArrayList<>();
			departments = CommonUtility.getListFromString(person.getUnit().getDepartments());
			facilityLocations=new ArrayList<>();
			facilityLocations = CommonUtility.getListFromString(person.getUnit().getFacilityLocation());

			
		}
	}
	
	/**
	 * @return the departments
	 */
	public List<String> getDepartments() {
		return departments;
	}

	public List<String> getFacilityLocations() {
		return facilityLocations;
	}

	public void setFacilityLocations(List<String> facilityLocations) {
		this.facilityLocations = facilityLocations;
	}

	/**
	 * @param departments
	 *            the departments to set
	 */
	public void setDepartments(List<String> departments) {
		this.departments = departments;
	}

}
