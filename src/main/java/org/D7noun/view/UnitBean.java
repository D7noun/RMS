package org.D7noun.view;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Init;
import javax.enterprise.context.Initialized;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Named;

import org.D7noun.facade.UnitFacade;
import org.D7noun.model.Unit;

/**
 * Backing bean for Unit entities.
 * <p/>
 * This class provides CRUD functionality for all Unit entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD
 * framework or custom base class.
 */

@Named
@SessionScoped
public class UnitBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private UnitFacade unitFacade;

	private Long id;

	

	
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	private Unit unit;

	public Unit getUnit() {
		return this.unit;
	}

	public void setUnit(Unit unit) {
		this.unit = unit;
	}

	public String create() {

		this.id=null;
		return "create?faces-redirect=true";
	}

	public void retrieve() {

		if (FacesContext.getCurrentInstance().isPostback()) {
			return;
		}

		if (this.id == null) {
			this.unit = this.example;
		} else {
			this.unit = findById(getId());
		}
	}

	public Unit findById(Long id) {

		return unitFacade.find(id);
	}

	/*
	 * Support updating and deleting Unit entities
	 */

	public String update() {

		try {
			unitFacade.save(this.unit);
			 this.example = new Unit();
			return "search?faces-redirect=true";
		} catch (Exception e) {
			if (e.getCause() != null && e.getCause().getCause() != null && e.getCause().getCause().getCause() != null) {

				if (e.getCause().getCause().getCause().getMessage()
						.contains("unit_uid_key"))
					FacesContext.getCurrentInstance().addMessage(null,
							new FacesMessage("there is already a unit with this Uid"));
				else if (e.getCause().getCause().getCause().getMessage()
						.contains("unit_name_key"))
					FacesContext.getCurrentInstance().addMessage(null,
							new FacesMessage("there is already a unit with this name"));
				else
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getMessage()));
			} else {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getMessage()));
			}
			return null;
		}
	}

	public String delete() {

		try {
			Unit deletableEntity = findById(getId());
			if (!deletableEntity.getPersons().isEmpty()) {
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage("you cant delete this unit,it has related people"));
				return null;
			}

			else {
				unitFacade.remove(deletableEntity);
				return "search?faces-redirect=true";
			}
		} catch (

		Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getMessage()));
			return null;
		}
	}

	/*
	 * Support searching Unit entities with pagination
	 */

	private int page;
	private long count;
	private List<Unit> pageItems;

	private Unit example = new Unit();

	public int getPage() {
		return this.page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getPageSize() {
		return 10;
	}

	public Unit getExample() {
		return this.example;
	}

	public void setExample(Unit example) {
		this.example = example;
	}

	public String search() {
		this.page = 0;
		return null;
	}
	public String clearSearch() {
		this.example=new Unit();
		return null;
	}
	public void paginate() {

		this.count = unitFacade.getCount(this.example);

		this.pageItems = unitFacade.getPaginatedList(this.page, getPageSize(), this.example);

	}

	public List<Unit> getPageItems() {
		return this.pageItems;
	}

	public long getCount() {
		return this.count;
	}

	public List<Unit> getAll() {

		return unitFacade.getAll();
	}

	public Converter getConverter() {

		return new Converter() {

			@Override
			public Object getAsObject(FacesContext context, UIComponent component, String value) {

				return unitFacade.find(Long.valueOf(value));
			}

			@Override
			public String getAsString(FacesContext context, UIComponent component, Object value) {

				if (value == null) {
					return "";
				}

				return String.valueOf(((Unit) value).getId());
			}
		};
	}

	private Unit add = new Unit();

	public Unit getAdd() {
		return this.add;
	}

	public Unit getAdded() {
		Unit added = this.add;
		this.add = new Unit();
		return added;
	}
}
