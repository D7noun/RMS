package org.D7noun.view;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Named;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.D7noun.facade.AttachmentRentalCardFacade;
import org.D7noun.facade.ItemFacade;
import org.D7noun.facade.PersonFacade;
import org.D7noun.facade.RentalCardFacade;
import org.D7noun.model.AttachmentRentalCard;
import org.D7noun.model.Item;
import org.D7noun.model.Item.State;
import org.D7noun.model.Person;
import org.D7noun.model.RentalCard;
import org.omnifaces.util.Faces;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.util.JRLoader;

/**
 * Backing bean for RentalCard entities.
 * <p/>
 * This class provides CRUD functionality for all RentalCard entities. It
 * focuses purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt>
 * for state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD
 * framework or custom base class.
 */

@Named
@SessionScoped
public class RentalCardBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private RentalCardFacade facade;
	@EJB
	private ItemFacade itemFacade;
	@EJB
	private PersonFacade personFacade;
	private Long id;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	private RentalCard rentalCard;

	public RentalCard getRentalCard() {
		return this.rentalCard;
	}

	public void setRentalCard(RentalCard rentalCard) {
		this.rentalCard = rentalCard;
	}

	public String create() {
		this.id = null;

		return "create?faces-redirect=true";
	}

	public void retrieve() {

		if (FacesContext.getCurrentInstance().isPostback()) {
			return;
		}

		if (this.id == null) {
			this.rentalCard = this.example;
		} else {
			this.rentalCard = findById(getId());
		}
	}

	public RentalCard findById(Long id) {
		return facade.find(id);
	}

	public List<RentalCard> getRentalCardsByItem(Item item) {
		return facade.getRentalCardsByItem(item);
	}

	public void deleteRentalCardsByItem(Item item) {
		facade.deleteRentalCardsByItem(item);
	}

	/*
	 * Support updating and deleting RentalCard entities
	 */

	public String update() {

		try {

			facade.save(this.rentalCard);
			if (this.rentalCard.getEndDate().before(new Date())) {
				this.rentalCard.getItem().setState(State.available);
			} else {
				this.rentalCard.getItem().setState(State.rented);
			}
			itemFacade.save(this.rentalCard.getItem());
			return "search?faces-redirect=true";

		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getMessage()));
			return null;
		}
	}

	public String delete() {

		try {
			RentalCard deletableEntity = findById(getId());
			Person user = deletableEntity.getUser();
			user.setRentalCard(null);
			personFacade.save(user);
			facade.remove(deletableEntity);
			return "search?faces-redirect=true";
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getMessage()));
			return null;
		}
	}

	/*
	 * Support searching RentalCard entities with pagination
	 */

	private int page;
	private long count;
	private List<RentalCard> pageItems;

	private RentalCard example = new RentalCard();

	public int getPage() {
		return this.page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getPageSize() {
		return 10;
	}

	public RentalCard getExample() {
		return this.example;
	}

	public void setExample(RentalCard example) {
		this.example = example;
	}

	public String search() {
		this.page = 0;
		return null;
	}

	public void paginate() {

		this.count = facade.getCount(this.example);

		this.pageItems = facade.getPaginatedList(this.page, getPageSize(), this.example);

	}

	public List<RentalCard> getPageItems() {
		return this.pageItems;
	}

	public long getCount() {
		return this.count;
	}

	/*
	 * Support listing and POSTing back RentalCard entities (e.g. from inside an
	 * HtmlSelectOneMenu)
	 */

	public List<RentalCard> getAll() {

		return facade.getAll();
	}

	public Converter getConverter() {

		return new Converter() {

			@Override
			public Object getAsObject(FacesContext context, UIComponent component, String value) {

				return facade.find(Long.valueOf(value));
			}

			@Override
			public String getAsString(FacesContext context, UIComponent component, Object value) {

				if (value == null) {
					return "";
				}

				return String.valueOf(((RentalCard) value).getId());
			}
		};
	}

	/*
	 * Support adding children to bidirectional, one-to-many tables
	 */

	/**
	 * D7noun
	 */

	@EJB
	private PersonFacade personBean;


	public void handleFileUpload(FileUploadEvent event) {
		try {

			UploadedFile uploadedFile = event.getFile();

			AttachmentRentalCard attachment = new AttachmentRentalCard();
			attachment.setRentalCard(rentalCard);
			attachment.setFileName(uploadedFile.getFileName());
			attachment.setSize(uploadedFile.getSize());
			attachment.setContentType(uploadedFile.getContentType());
			attachment.setData(uploadedFile.getContents());
			rentalCard.getAttachments().add(attachment);
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("RentalCardBean: handleFileUpload");
		}
	}

	public void download(AttachmentRentalCard attachment) {

		try {
			Faces.sendFile(attachment.getData(), attachment.getFileName(), true);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@EJB
	private AttachmentRentalCardFacade attachmentFacade;
	private AttachmentRentalCard attachmentFordelete;

	public void selectForDelete(AttachmentRentalCard attachment) {
		attachmentFordelete = attachment;
	}

	public void deleteAttachment() {
		try {
			rentalCard.getAttachments().remove(attachmentFordelete);
			attachmentFacade.removeAttachmentFromTable(rentalCard, attachmentFordelete);
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}
	public String clearSearch() {
		this.example=new RentalCard();
		return null;
	}
	public void print1() {
		try {
			// compile the report from the stream

			HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext()
					.getResponse();
			response.reset();
			response.setContentType("application/pdf");
			response.setHeader("Content-disposition", "attachment; filename=\"report.pdf\"");

			ClassLoader classloader = Thread.currentThread().getContextClassLoader();
			InputStream inputStream = classloader.getResourceAsStream("report.jasper");
			JasperReport report = (JasperReport) JRLoader.loadObject(inputStream);
			// fill out the report into a print object, ready for export.
			Map<String, Object> parameters = new HashMap<>();

			parameters.put("startDate", rentalCard.getStartDate());
			parameters.put("dueDate", rentalCard.getDueDate());
			parameters.put("endDate", rentalCard.getEndDate());
			parameters.put("item", rentalCard.getItem());
			parameters.put("user", rentalCard.getUser());
			parameters.put("note", rentalCard.getNote());

			JasperPrint print = JasperFillManager.fillReport(report, parameters, new JREmptyDataSource(1));
			// export it!
			JasperExportManager.exportReportToPdfStream(print, response.getOutputStream());
			FacesContext.getCurrentInstance().responseComplete();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void print() {
		try {
			HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext()
					.getResponse();

			ServletOutputStream servletOutputStream = response.getOutputStream();

			ClassLoader classloader = Thread.currentThread().getContextClassLoader();
			InputStream is = classloader.getResourceAsStream("rental-card.jrxml");
			JasperReport report = JasperCompileManager.compileReport(is);

			Map<String, Object> parameters = new HashMap<>();

			parameters.put("startDate", getStringDateFromDate(rentalCard.getStartDate()));
			parameters.put("dueDate", getStringDateFromDate(rentalCard.getDueDate()));
			parameters.put("endDate", getStringDateFromDate(rentalCard.getEndDate()));
			parameters.put("item", rentalCard.getItem().displayName());
			parameters.put("user", rentalCard.getUser().getName());
			parameters.put("note", rentalCard.getNote());

			byte[] bytes = null;
			bytes = JasperRunManager.runReportToPdf(report, parameters, new JREmptyDataSource(1));

			response.setContentType("application/pdf");
			response.setContentLength(bytes.length);

			servletOutputStream.write(bytes, 0, bytes.length);
			servletOutputStream.flush();
			servletOutputStream.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private String getStringDateFromDate(Date date) {
		if (date == null) {
			return "";
		}
		return new SimpleDateFormat("dd/MM/yyyy").format(date);

	}

	/**
	 * 
	 * */

	private RentalCard add = new RentalCard();

	public RentalCard getAdd() {
		return this.add;
	}

	public RentalCard getAdded() {
		RentalCard added = this.add;
		this.add = new RentalCard();
		return added;
	}

}