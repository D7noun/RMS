package org.D7noun.view;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.D7noun.facade.AttachmentDamagedFacade;
import org.D7noun.facade.DamageFacade;
import org.D7noun.model.AttachmentDamaged;
import org.D7noun.model.Damaged;
import org.D7noun.model.Item;
import org.D7noun.model.Item.State;
import org.D7noun.model.Maintainence;

import org.omnifaces.util.Faces;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;

/**
 * Backing bean for Damaged entities.
 * <p/>
 * This class provides CRUD functionality for all Damaged entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD
 * framework or custom base class.
 */
@Named
@SessionScoped
public class DamagedBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private DamageFacade facade;

	private Long id;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	private Damaged damaged;

	public Damaged getDamaged() {
		return this.damaged;
	}

	public void setDamaged(Damaged damaged) {
		this.damaged = damaged;
	}

	@Inject
	private Conversation conversation;

	@PersistenceContext(unitName = "ems-pu", type = PersistenceContextType.EXTENDED)
	private EntityManager entityManager;

	public String create() {

		this.id = null;

		return "create?faces-redirect=true";
	}

	public void retrieve() {

		if (FacesContext.getCurrentInstance().isPostback()) {
			return;
		}

		if (this.conversation.isTransient()) {
			this.conversation.begin();
			this.conversation.setTimeout(1800000L);
		}

		if (this.id == null) {
			this.damaged = this.example;
		} else {
			this.damaged = findById(getId());
		}
	}

	public Damaged findById(Long id) {
		return this.entityManager.find(Damaged.class, id);
	}
	public String clearSearch() {
		this.example=new Damaged();
		return null;
	}
	@SuppressWarnings("unchecked")
	public List<Damaged> getDamagedCardsByItem(Item item) {
		try {
			List<Damaged> result = new ArrayList<>();
			Query query = this.entityManager.createNamedQuery(Damaged.getDamagedCardsByItemId, Damaged.class);
			query.setParameter("item", item);
			result = query.getResultList();
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: DamagedBean.getDamagedCardsByItem");
		}
		return new ArrayList<>();
	}

	public void deleteDamagedCardsByItem(Item item) {
		try {
			Query query = this.entityManager.createNamedQuery(Maintainence.deleteMaintenanceCardsByItem,
					Maintainence.class);
			query.setParameter("item", item);
			query.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("MaintenanceBean.deleteMaintenanceCardsByItem");
		}
	}

	/*
	 * Support updating and deleting Damaged entities
	 */

	public String update() {
		this.conversation.end();

		try {
			if (this.id == null) {
				this.entityManager.persist(this.damaged);
				this.damaged.getItem().setState(State.damaged);
				this.entityManager.merge(this.damaged.getItem());
				return "search?faces-redirect=true";
			} else {
				this.entityManager.merge(this.damaged);
				this.damaged.getItem().setState(State.damaged);
				this.entityManager.merge(this.damaged.getItem());
				return "search?faces-redirect=true";
			}
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getMessage()));
			return null;
		}
	}

	public String delete() {
		this.conversation.end();

		try {
			Damaged deletableEntity = findById(getId());
			Iterator<AttachmentDamaged> iterAttachments = deletableEntity.getAttachments().iterator();
			for (; iterAttachments.hasNext();) {
				AttachmentDamaged nextInAttachments = iterAttachments.next();
				nextInAttachments.setDamaged(null);
				iterAttachments.remove();
				this.entityManager.merge(nextInAttachments);
			}
			this.entityManager.remove(deletableEntity);
			this.entityManager.flush();
			return "search?faces-redirect=true";
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getMessage()));
			return null;
		}
	}

	/*
	 * Support searching Damaged entities with pagination
	 */

	private int page;
	private long count;
	private List<Damaged> pageItems;

	private Damaged example = new Damaged();

	public int getPage() {
		return this.page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getPageSize() {
		return 10;
	}

	public Damaged getExample() {
		return this.example;
	}

	public void setExample(Damaged example) {
		this.example = example;
	}

	public String search() {
		this.page = 0;
		return null;
	}

	public void paginate() {

		this.count = facade.getCount(this.example);

		this.pageItems = facade.getPaginatedList(this.page, getPageSize(), this.example);

	}

	public List<Damaged> getPageItems() {
		return this.pageItems;
	}

	public long getCount() {
		return this.count;
	}

	/*
	 * Support listing and POSTing back Damaged entities (e.g. from inside an
	 * HtmlSelectOneMenu)
	 */

	public List<Damaged> getAll() {

		CriteriaQuery<Damaged> criteria = this.entityManager.getCriteriaBuilder().createQuery(Damaged.class);
		return this.entityManager.createQuery(criteria.select(criteria.from(Damaged.class))).getResultList();
	}

	public Converter getConverter() {

		return new Converter() {

			@Override
			public Object getAsObject(FacesContext context, UIComponent component, String value) {

				return facade.find(Long.valueOf(value));
			}

			@Override
			public String getAsString(FacesContext context, UIComponent component, Object value) {

				if (value == null) {
					return "";
				}

				return String.valueOf(((Damaged) value).getId());
			}
		};
	}

	/*
	 * Support adding children to bidirectional, one-to-many tables
	 */

	private Damaged add = new Damaged();

	public Damaged getAdd() {
		return this.add;
	}

	public Damaged getAdded() {
		Damaged added = this.add;
		this.add = new Damaged();
		return added;
	}

	/**
	 * 
	 * D7noun
	 * 
	 **/

	public void handleFileUpload(FileUploadEvent event) {
		try {

			UploadedFile uploadedFile = event.getFile();

			AttachmentDamaged attachment = new AttachmentDamaged();
			attachment.setDamaged(damaged);
			attachment.setFileName(uploadedFile.getFileName());
			attachment.setSize(uploadedFile.getSize());
			attachment.setContentType(uploadedFile.getContentType());
			attachment.setData(uploadedFile.getContents());
			damaged.getAttachments().add(attachment);
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("MaintenanceBean: handleFileUpload");
		}
	}

	public void download(AttachmentDamaged attachment) {

		try {
			Faces.sendFile(attachment.getData(), attachment.getFileName(), true);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@EJB
	private AttachmentDamagedFacade attachmentFacade;
	private AttachmentDamaged attachmentFordelete;

	public void selectForDelete(AttachmentDamaged attachment) {
		attachmentFordelete = attachment;
	}

	public void deleteAttachment() {
		try {
			damaged.getAttachments().remove(attachmentFordelete);
			attachmentFacade.removeAttachmentFromTable(damaged, attachmentFordelete);
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}

	public void print() {
		try {
			HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext()
					.getResponse();

			ServletOutputStream servletOutputStream = response.getOutputStream();

			ClassLoader classloader = Thread.currentThread().getContextClassLoader();
			InputStream is = classloader.getResourceAsStream("damaged.jrxml");
			JasperReport report = JasperCompileManager.compileReport(is);

			Map<String, Object> parameters = new HashMap<>();

			parameters.put("date", getStringDateFromDate(damaged.getDate()));
			parameters.put("item", damaged.getItem().displayName());
			parameters.put("reason", damaged.getReason());
			if (damaged.getPersonInCharge() != null) {
				parameters.put("person", damaged.getPersonInCharge().getName());
			} else {
				parameters.put("person", "");
			}

			byte[] bytes = null;
			bytes = JasperRunManager.runReportToPdf(report, parameters, new JREmptyDataSource(1));

			response.setContentType("application/pdf");
			response.setContentLength(bytes.length);

			servletOutputStream.write(bytes, 0, bytes.length);
			servletOutputStream.flush();
			servletOutputStream.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private String getStringDateFromDate(Date date) {
		if (date == null) {
			return "";
		}
		return new SimpleDateFormat("dd/MM/yyyy").format(date);

	}
}
