package org.D7noun.view;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Named;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.D7noun.facade.AttachmentMaintenanceFacade;
import org.D7noun.facade.ItemFacade;
import org.D7noun.facade.MaintenanceFacade;
import org.D7noun.model.AttachmentMaintenance;
import org.D7noun.model.Item;
import org.D7noun.model.Item.State;
import org.D7noun.model.Maintainence;

import org.D7noun.util.CommonUtility;
import org.omnifaces.util.Faces;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;

/**
 * Backing bean for Maintainence entities.
 * <p/>
 * This class provides CRUD functionality for all Maintainence entities. It
 * focuses purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt>
 * for state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD
 * framework or custom base class.
 */

@Named
@SessionScoped
public class MaintainenceBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private MaintenanceFacade facade;

	@EJB
	private ItemFacade itemFacade;
	private Long id;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	private Maintainence maintainence;

	public Maintainence getMaintainence() {
		return this.maintainence;
	}

	public void setMaintainence(Maintainence maintainence) {
		this.maintainence = maintainence;
	}

	public String create() {

		this.id = null;

		return "create?faces-redirect=true";
	}

	public void retrieve() {

		if (FacesContext.getCurrentInstance().isPostback()) {
			return;
		}

		if (this.id == null) {
			this.maintainence = this.example;
		} else {
			this.maintainence = findById(getId());
			fillEmployeesFromCompany();
		}
	}

	public Maintainence findById(Long id) {
		return facade.find(id);
	}

	public String clearSearch() {
		this.example=new Maintainence();
		return null;
	}
	public List<Maintainence> getMaintenanceCardsByItem(Item item) {
		return facade.getMaintenanceCardsByItem(item);
	}

	public void deleteMaintenanceCardsByItem(Item item) {
		facade.deleteMaintenanceCardsByItem(item);
	}

	/*
	 * Support updating and deleting Maintainence entities
	 */

	public String update() {

		try {
			facade.save(this.maintainence);

			if (this.maintainence.getEndDate().before(new Date())) {
				this.maintainence.getItem().setState(State.available);
			} else {
				this.maintainence.getItem().setState(State.under_maintenance);
			}
			itemFacade.save(this.maintainence.getItem());

			return "search?faces-redirect=true";

		} catch (

		Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getMessage()));
			return null;
		}
	}

	public String delete() {

		try {
			Maintainence deletableEntity = findById(getId());
			facade.remove(deletableEntity);
			return "search?faces-redirect=true";
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getMessage()));
			return null;
		}
	}

	/*
	 * Support searching Maintainence entities with pagination
	 */

	private int page;
	private long count;
	private List<Maintainence> pageItems;

	private Maintainence example = new Maintainence();

	public int getPage() {
		return this.page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getPageSize() {
		return 10;
	}

	public Maintainence getExample() {
		return this.example;
	}

	public void setExample(Maintainence example) {
		this.example = example;
	}

	public String search() {
		this.page = 0;
		return null;
	}

	public void paginate() {

		this.count = facade.getCount(this.example);

		this.pageItems = facade.getPaginatedList(this.page, getPageSize(), this.example);

	}

	public List<Maintainence> getPageItems() {
		return this.pageItems;
	}

	public long getCount() {
		return this.count;
	}

	/*
	 * Support listing and POSTing back Maintainence entities (e.g. from inside
	 * an HtmlSelectOneMenu)
	 */

	public List<Maintainence> getAll() {

		return facade.getAll();
	}

	public Converter getConverter() {


		return new Converter() {

			@Override
			public Object getAsObject(FacesContext context, UIComponent component, String value) {

				return facade.find(Long.valueOf(value));
			}

			@Override
			public String getAsString(FacesContext context, UIComponent component, Object value) {

				if (value == null) {
					return "";
				}

				return String.valueOf(((Maintainence) value).getId());
			}
		};
	}

	/*
	 * Support adding children to bidirectional, one-to-many tables
	 */

	private Maintainence add = new Maintainence();

	public Maintainence getAdd() {
		return this.add;
	}

	public Maintainence getAdded() {
		Maintainence added = this.add;
		this.add = new Maintainence();
		return added;
	}

	/**
	 * 
	 * D7noun
	 * 
	 **/

	public void handleFileUpload(FileUploadEvent event) {
		try {

			UploadedFile uploadedFile = event.getFile();

			AttachmentMaintenance attachment = new AttachmentMaintenance();
			attachment.setMaintainence(maintainence);
			attachment.setFileName(uploadedFile.getFileName());
			attachment.setSize(uploadedFile.getSize());
			attachment.setContentType(uploadedFile.getContentType());
			attachment.setData(uploadedFile.getContents());
			maintainence.getAttachments().add(attachment);
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("MaintenanceBean: handleFileUpload");
		}
	}

	public void download(AttachmentMaintenance attachment) {

		try {
			Faces.sendFile(attachment.getData(), attachment.getFileName(), true);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@EJB
	private AttachmentMaintenanceFacade attachmentFacade;
	private AttachmentMaintenance attachmentFordelete;

	public void selectForDelete(AttachmentMaintenance attachment) {
		attachmentFordelete = attachment;
	}

	public void deleteAttachment() {
		try {
			maintainence.getAttachments().remove(attachmentFordelete);
			attachmentFacade.removeAttachmentFromTable(maintainence, attachmentFordelete);
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}

	public void print() {
		try {
			HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext()
					.getResponse();

			ServletOutputStream servletOutputStream = response.getOutputStream();

			ClassLoader classloader = Thread.currentThread().getContextClassLoader();
			InputStream is = classloader.getResourceAsStream("maintenance.jrxml");
			JasperReport report = JasperCompileManager.compileReport(is);

			Map<String, Object> parameters = new HashMap<>();

			parameters.put("startDate", getStringDateFromDate(maintainence.getStartDate()));
			parameters.put("dueDate", getStringDateFromDate(maintainence.getDueDate()));
			parameters.put("endDate", getStringDateFromDate(maintainence.getEndDate()));
			parameters.put("item", maintainence.getItem().displayName());
			parameters.put("malfunctioning", maintainence.getMalfunctioning());
			parameters.put("fees", maintainence.getFees());
			parameters.put("maintenanceMan", maintainence.getMaintenanceMan());

			byte[] bytes = null;
			bytes = JasperRunManager.runReportToPdf(report, parameters, new JREmptyDataSource(1));

			response.setContentType("application/pdf");
			response.setContentLength(bytes.length);

			servletOutputStream.write(bytes, 0, bytes.length);
			servletOutputStream.flush();
			servletOutputStream.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private String getStringDateFromDate(Date date) {
		if (date == null) {
			return "";
		}
		return new SimpleDateFormat("dd/MM/yyyy").format(date);

	}

	/**
	 * 
	 * D7noun
	 * 
	 */

	private List<String> employees = new ArrayList<String>();

	public void fillEmployeesFromCompany() {
		if (maintainence != null && maintainence.getMaintenanceCompany() != null) {
			employees = CommonUtility.getListFromString(maintainence.getMaintenanceCompany().getEmployees());
		}
	}

	/**
	 * @return the employees
	 */
	public List<String> getEmployees() {
		return employees;
	}

	/**
	 * @param employees
	 *            the employees to set
	 */
	public void setEmployees(List<String> employees) {
		this.employees = employees;
	}
}
