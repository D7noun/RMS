package org.D7noun.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.D7noun.dto.ItemDto;
import org.D7noun.facade.ItemFacade;
import org.D7noun.model.Item;


import com.google.gson.Gson;

/**
 * Servlet implementation class ItemServlet
 */
@WebServlet("/ItemServlet")
public class ItemServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@EJB
	private ItemFacade itemBean;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ItemServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String data[] = new String[2];

		String id = request.getParameter("id");

		Gson gson = new Gson();

		List<Item> items = new ArrayList<Item>();
		List<ItemDto> itemsDto = new ArrayList<ItemDto>();

		List<Item> itemsAdded = new ArrayList<Item>();
		List<ItemDto> itemsAddedDto = new ArrayList<ItemDto>();

		items = itemBean.getAllAvailableItems((long)0);
		Item itemSend = itemBean.find(Long.parseLong(id));

		if (itemSend != null) {
			if (itemSend.getSerialNumber() != null) {
				itemsAdded = itemBean.getAccessoriesByItemSerialNumber(itemSend.getSerialNumber());
			} else {
				itemsAdded = null;
			}
		} else {
			itemsAdded = null;
		}

		if (items != null && !items.isEmpty()) {
			if (itemSend != null) {
				items.remove(itemSend);
			}
			for (Item item : items) {
				itemsDto.add(new ItemDto(item.getDeviceId(), item.getName(), item.getModel(), item.getSerialNumber()));
			}
		}

		if (itemsAdded != null && !itemsAdded.isEmpty()) {
			for (Item item : itemsAdded) {
				itemsAddedDto
						.add(new ItemDto(item.getDeviceId(), item.getName(), item.getModel(), item.getSerialNumber()));
			}
		}

		data[0] = gson.toJson(itemsDto);
		data[1] = gson.toJson(itemsAddedDto);

		response.getWriter().write(gson.toJson(data));

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
