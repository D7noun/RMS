$(document).ready(function() {
	initEmployees();
});

function initEmployees() {
	$('.companyBeanManufacturerEmployees').selectize({
		delimiter : ',',
		persist : false,
		create : function(input) {
			return {
				value : input,
				text : input
			}
		}
	});
}