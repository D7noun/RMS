$(document).ready(function() {
	initSpecilities();
	initUsages();
	initDomains();
	initTypes();
//	initAccessories();
});

function initAccessories() {

	var options = [];
	var items = [];

	$
			.ajax({
				url : "/EMS/ItemServlet",
				data : {
					id : location.search.split('=')[1]
				},
				type : "GET",
				success : function(data, status, object) {
					var json = JSON.parse(data);

					$.each(JSON.parse(json[0]), function(i, val) {
						options[i] = {
							name : val.name,
							serialNumber : val.serialNumber
						}
					})

					$.each(JSON.parse(json[1]), function(i, val) {
						items[i] = val.serialNumber;
					})

					$('#itemBeanItemAccessories')
							.selectize(
									{
										persist : false,
										maxItems : null,
										valueField : 'serialNumber',
										labelField : 'name',
										searchField : [ 'serialNumber', 'name' ],
										options : options,
										items : items,
										render : {
											item : function(item, escape) {
												return '<div>'
														+ (item.name ? '<span class="name">'
																+ escape(item.name)
																+ '</span>'
																: '')
														+ (item.serialNumber ? '<span class="serialNumber">'
																+ escape(item.serialNumber)
																+ '</span>'
																: '')
														+ '</div>';
											},
											option : function(item, escape) {
												var label = item.name
														|| item.serialNumber;
												var caption = item.name ? item.serialNumber
														: null;
												return '<div>'
														+ '<span class="label">'
														+ escape(label)
														+ '</span>'
														+ (caption ? '<span class="caption">'
																+ escape(caption)
																+ '</span>'
																: '')
														+ '</div>';
											}
										}
									});

				},
				failure : function(data, status, object) {
					console.debug('failure');
				}

			});

}

function initSpecilities() {
	var options = [];
	var items = [];

	$.ajax({
		url : "/EMS/SettingsServlet",
		data : {
			type : "specilities"
		},
		type : "GET",
		success : function(data, status, object) {
			var json = JSON.parse(data);

			$.each(JSON.parse(json[0]), function(i, val) {
				options[i] = {
					name : val
				}
			})

			$('#itemBeanItemSpecilities').selectize(
					{
						delimiter : ',',
						persist : false,
						valueField : 'name',
						labelField : 'name',
						searchField : 'name',
						options : options,
						create : function(input) {
							return {
								value : input,
								text : input
							}
						},
						render : {
							item : function(item, escape) {
								return '<div>'
										+ (item.name ? '<span class="name">'
												+ escape(item.name) + '</span>'
												: '') + '</div>';
							},
							option : function(item, escape) {
								var label = item.name;
								return '<div>' + '<span class="label">'
										+ escape(label) + '</span></div>';
							}
						},
					});

		},
		failure : function(data, status, object) {
			console.debug('failure');
		}

	});
}

function initUsages() {
	var options = [];
	var items = [];

	$.ajax({
		url : "/EMS/SettingsServlet",
		data : {
			// id : location.search.split('=')[1]
			type : "usages"
		},
		type : "GET",
		success : function(data, status, object) {
			var json = JSON.parse(data);

			$.each(JSON.parse(json[0]), function(i, val) {
				options[i] = {
					name : val
				}
			})

			$('#itemBeanItemUsages').selectize(
					{
						delimiter : ',',
						persist : false,
						valueField : 'name',
						labelField : 'name',
						searchField : 'name',
						options : options,
						create : function(input) {
							return {
								value : input,
								text : input
							}
						},
						render : {
							item : function(item, escape) {
								return '<div>'
										+ (item.name ? '<span class="name">'
												+ escape(item.name) + '</span>'
												: '') + '</div>';
							},
							option : function(item, escape) {
								var label = item.name;
								return '<div>' + '<span class="label">'
										+ escape(label) + '</span></div>';
							}
						},
					});

		},
		failure : function(data, status, object) {
			console.debug('failure');
		}

	});
}

function initDomains() {
	var options = [];
	var items = [];

	$.ajax({
		url : "/EMS/SettingsServlet",
		data : {
			// id : location.search.split('=')[1]
			type : "domains"
		},
		type : "GET",
		success : function(data, status, object) {
			var json = JSON.parse(data);

			$.each(JSON.parse(json[0]), function(i, val) {
				options[i] = {
					name : val
				}
			})

			$('#itemBeanItemDomains').selectize(
					{
						delimiter : ',',
						persist : false,
						valueField : 'name',
						labelField : 'name',
						searchField : 'name',
						options : options,
						create : function(input) {
							return {
								value : input,
								text : input
							}
						},
						render : {
							item : function(item, escape) {
								return '<div>'
										+ (item.name ? '<span class="name">'
												+ escape(item.name) + '</span>'
												: '') + '</div>';
							},
							option : function(item, escape) {
								var label = item.name;
								return '<div>' + '<span class="label">'
										+ escape(label) + '</span></div>';
							}
						},
					});

		},
		failure : function(data, status, object) {
			console.debug('failure');
		}

	});
}

function initTypes() {
	var options = [];
	var items = [];

	$.ajax({
		url : "/EMS/SettingsServlet",
		data : {
			// id : location.search.split('=')[1]
			type : "types"
		},
		type : "GET",
		success : function(data, status, object) {
			var json = JSON.parse(data);

			$.each(JSON.parse(json[0]), function(i, val) {
				options[i] = {
					name : val
				}
			})

			$('#itemBeanItemTypes').selectize(
					{
						delimiter : ',',
						persist : false,
						valueField : 'name',
						labelField : 'name',
						searchField : 'name',
						options : options,
						create : function(input) {
							return {
								value : input,
								text : input
							}
						},
						render : {
							item : function(item, escape) {
								return '<div>'
										+ (item.name ? '<span class="name">'
												+ escape(item.name) + '</span>'
												: '') + '</div>';
							},
							option : function(item, escape) {
								var label = item.name;
								return '<div>' + '<span class="label">'
										+ escape(label) + '</span></div>';
							}
						},
					});

		},
		failure : function(data, status, object) {
			console.debug('failure');
		}

	});
}
