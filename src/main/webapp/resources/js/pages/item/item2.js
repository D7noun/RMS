$(document)
		.ready(
				function() {

					var options = [];
					var items = [];

					$
							.ajax({
								url : "/EMS/ItemServlet",
								data : {
									id : location.search.split('=')[1]
								},
								type : "GET",
								success : function(data, status, object) {
									var json = JSON.parse(data);

									$
											.each(
													JSON.parse(json[0]),
													function(i, val) {
														options[i] = {
															name : val.name,
															serialNumber : val.serialNumber
														}
													})

									$.each(JSON.parse(json[1]),
											function(i, val) {
												items[i] = val.serialNumber;
											})

									$('#itemBeanItemAccessories')
											.selectize(
													{
														persist : false,
														maxItems : null,
														valueField : 'serialNumber',
														labelField : 'name',
														searchField : [
																'serialNumber',
																'name' ],
														options : options,
														items : items,
														render : {
															item : function(
																	item,
																	escape) {
																return '<div>'
																		+ (item.name ? '<span class="name">'
																				+ escape(item.name)
																				+ '</span>'
																				: '')
																		+ (item.serialNumber ? '<span class="serialNumber">'
																				+ escape(item.serialNumber)
																				+ '</span>'
																				: '')
																		+ '</div>';
															},
															option : function(
																	item,
																	escape) {
																var label = item.name
																		|| item.serialNumber;
																var caption = item.name ? item.serialNumber
																		: null;
																return '<div>'
																		+ '<span class="label">'
																		+ escape(label)
																		+ '</span>'
																		+ (caption ? '<span class="caption">'
																				+ escape(caption)
																				+ '</span>'
																				: '')
																		+ '</div>';
															}
														}
													});

								},
								failure : function(data, status, object) {
									console.debug('failure');
								}

							});

				});

var initSelectize
{

	accessories = function() {

	}

}
