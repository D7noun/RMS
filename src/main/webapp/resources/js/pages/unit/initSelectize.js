$(document).ready(function() {

	$('#unitBeanDepartments').selectize({
		delimiter : ',',
		persist : false,
		create : function(input) {
			return {
				value : input,
				text : input
			}
		}
	});

	$('#unitBeanUnitFacilityLocation').selectize({
		delimiter : ',',
		persist : false,
		create : function(input) {
			return {
				value : input,
				text : input
			}
		}
	});

})