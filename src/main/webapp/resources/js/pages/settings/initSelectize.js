$(document).ready(function() {

	$('#Specility').selectize({
		delimiter : ',',
		persist : false,
		create : function(input) {
			return {
				value : input,
				text : input
			}
		}
	});

	$('#Usage').selectize({
		delimiter : ',',
		persist : false,
		create : function(input) {
			return {
				value : input,
				text : input
			}
		}
	});

	$('#Domain').selectize({
		delimiter : ',',
		persist : false,
		create : function(input) {
			return {
				value : input,
				text : input
			}
		}
	});

	$('#Type').selectize({
		delimiter : ',',
		persist : false,
		create : function(input) {
			return {
				value : input,
				text : input
			}
		}
	});

})